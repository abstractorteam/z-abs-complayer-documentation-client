
'use strict';

const DataChartLineConfig = require('./data-chart-line-config');


class DataChartLine {
  constructor() {
    this.title = '';
    this.dataChartLineConfig = new DataChartLineConfig();
    this.axes = [];
    this.data = new Map();
    this.grid = null;
  }
  
  addTitle(title) {
    this.title = title;
  }
  
  addAxis(axis) {
    this.axes.push(axis);
  }
  
  setGrid(grid) {
    this.grid = grid;
  }
  
  addConfig(name, value) {
    this.dataChartLineConfig.addConfig(name, value);
  }
  
  getData(name) {
    return this.data.get(name);
  }
  
  addData(name, data) {
    this.data.set(name, data);
  }
}

module.exports = DataChartLine;
