
'use strict';

const DataConfig = require('../../data-config');


class DataChartLineConfig extends DataConfig {
  constructor() {
    super();
    this.width = 400;
    this.height = 200;
    this.widthBias = 25;
    this.heightBias = 25;
  }
}

module.exports = DataChartLineConfig;
