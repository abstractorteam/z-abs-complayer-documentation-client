
'use strict';


class DataFlowchartConnection {
  constructor(data, type, connectionType, fromBlock, toBlock) {
    this.data = data;
    this.type = type;
    this.connectionType = connectionType;
    this.fromBlockName = fromBlock ? fromBlock.name : '';
    this.toBlockName = toBlock ? toBlock.name : '';
    fromBlock && fromBlock.addOut();
    toBlock && toBlock.addIn();
  }
}

module.exports = DataFlowchartConnection;
