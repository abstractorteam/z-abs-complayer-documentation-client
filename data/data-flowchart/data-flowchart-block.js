
'use strict';


class DataFlowchartBlock {
  constructor(name, type, data, css, link) {
    this.name = name;
    this.type = undefined !== type ? type : 'action';
    this.data = data;
    this.css = css;
    this.link = link;
    this.xIndex = -1;
    this.yIndex = -1;
    this.in = 0;
    this.out = 0;
  }
  
  addIndex(xIndex, yIndex) {
    this.xIndex = xIndex;
    this.yIndex = yIndex;
  }
  
  addIn() {
    ++this.in;
  }
  
  addOut() {
    ++this.out;
  }
}


module.exports = DataFlowchartBlock;
