
'use strict';

class DataImage {
  constructor() {
    this.src = '';
    this.width = '';
    this.height = '';
    this.float = '';
  }
  
  addParameter(name, value) {
    const member = Reflect.get(this, name);
    if(undefined !== member) {
      Reflect.set(this, name, value);
    }
  }
}

module.exports = DataImage;
