
'use strict';


class DataSequenceDiagram {
  constructor(title = '', nodes = [], messages = []) {
    this.title = title;
    this.nodes = nodes;
    this.messages = messages;
  }
  
  setTitle(title) {
    this.title = title;    
  }
  
  addNode(node) {
    this.nodes.push(node);
  }

  addMessage(message) {
    this.messages.push(message);
  }
}

module.exports = DataSequenceDiagram;
