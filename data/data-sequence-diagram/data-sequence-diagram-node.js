
'use strict';


class DataSequenceDiagramNode {
  constructor(name) {
    this.name = name;
  }
}

module.exports = DataSequenceDiagramNode;
