
'use strict';


class DataSequenceDiagramMessage {
  constructor(data, type, messageType, fromIndex, toIndex) {
    this.data = data;
    this.type = type;
    this.messageType = messageType;
    this.fromIndex = fromIndex;
    this.toIndex = toIndex;
  }
}

module.exports = DataSequenceDiagramMessage;
