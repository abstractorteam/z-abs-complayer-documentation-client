
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataTestDataSystemGlobal extends DataTableKeyValue {
  constructor(name, value, description) {
    super();
    this.name = name;
    this.value = value;
    this.description = description;
  }
}

module.exports = DataTestDataSystemGlobal;
