
'use strict';


class DataLabInterfaceClient {
  constructor(name, network, type, address, netmask, isStatic, external, valid, reduced) {
    this.name = name ? name : 'actor1';
    this.network = network ? network : 'N1';
    this.type = type ? type : 'ip';
    this.address = address ? address : '192.168.0.1';
    this.netmask = netmask ? netmask : '255.255.0.0';
    this.static = isStatic ? ('true' === isStatic ? true : false) : false;
    this.external = external ? ('true' === external ? true : false) : false;
    this.valid = valid ? ('true' === valid ? true : false) : false;
    this.reduced = reduced ? ('true' === reduced ? true : false) : false;
  }
}

module.exports = DataLabInterfaceClient;
