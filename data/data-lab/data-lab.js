
'use strict';


class DataLab {
  constructor(networks) {
    this.networks = undefined !== networks ? networks : [];
    this.networksMap = new Map(undefined !== networks ? networks.map((network) => {return [network.name, network]}) : undefined);
  }
  
  addNetwork(network) {
    this.networks.push(network);
    this.networksMap.set(network.name, network);
  }
  
  getNetwork(name) {
    return this.networksMap.get(name);
  }
}

module.exports = DataLab;
