
'use strict';


class DataLabNetworks {
  constructor(name, family, description, valid, reduced) {
    this.name = name ? name : 'N1';
    this.family = 'IPv6' === family ? 'IPv6' : 'IPv4';
    this.description = description;
    this.valid = 'false' === valid ? false : true;
    this.reduced = 'true' === reduced ? true : false;
    this.clients = [];
    this.suts = [];
    this.servers = [];
  }
  
  addClient(client) {
    this.clients.push(client);
  }
  
  addSut(sut) {
    this.suts.push(sut);
  }
  
  addServer(server) {
    this.servers.push(server);
  }
}

module.exports = DataLabNetworks;
