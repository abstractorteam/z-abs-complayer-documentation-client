
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataAddressesNetworkInterfaceGlobal extends DataTableKeyValue {
  constructor(networkName, sut, family, description) {
    super();
    this.networkName = networkName;
    this.sut = sut;
    this.family = family;
    this.description = description;
  }
}

module.exports = DataAddressesNetworkInterfaceGlobal;
