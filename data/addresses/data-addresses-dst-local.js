
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataAddressesDstLocal extends DataTableKeyValue {
  constructor(addressName, interfaceName, sut, labId, userId, port, uri) {
    super();
    this.addressName = addressName;
    this.interfaceName = interfaceName;
    this.sut = sut;
    this.labId = labId;
    this.userId = userId;
    this.port = port;
    this.uri = uri;
  }
}

module.exports = DataAddressesDstLocal;
