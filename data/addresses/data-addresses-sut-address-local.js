
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataAddressesSutAddressLocal extends DataTableKeyValue {
  constructor(interfaceName, sut, labId, userId, address, netmask, external) {
    super();
    this.interfaceName = interfaceName;
    this.sut = sut;
    this.labId = labId;
    this.userId = userId;
    this.address = address;
    this.netmask = netmask;
    this.external = external;
  }
}

module.exports = DataAddressesSutAddressLocal;
