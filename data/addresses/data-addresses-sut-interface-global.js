
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataAddressesSutInterfaceGlobal extends DataTableKeyValue {
  constructor(interfaceName, sut, type, networkName, direction, description) {
    super();
    this.interfaceName = interfaceName;
    this.sut = sut;
    this.type = type;
    this.networkName = networkName;
    this.direction = direction;
    this.description = description;
  }
}

module.exports = DataAddressesSutInterfaceGlobal;
