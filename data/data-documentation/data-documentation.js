
'use strict';


class DataDocumentation {
  constructor(navigations = [], page) {
    this.navigations = [];
    this.page = page;
  }
  
  addNavigation(navigation) {
    this.navigations.push(navigation);
  }
  
  setPage(page) {
    this.page = page;
  }
}

module.exports = DataDocumentation;
