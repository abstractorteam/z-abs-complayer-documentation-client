
'use strict';


class DataData {
  constructor(name, numberType) {
    this.name = name;
    this.numberType = numberType;
    this.datas = [];
  }
  
  addData(...datas) {
    const parsedData = [];
    if('int' === this.numberType) {
      datas.forEach((data) => {
        parsedData.push(Number.parseInt(data));
      });
    }
    else if('float' === this.numberType) {
      datas.forEach((data) => {
        parsedData.push(Number.parseFloat(data));
      });
    }
    this.datas.push(parsedData);
  }
}

module.exports = DataData;
