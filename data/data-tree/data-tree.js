
'use strict';

const DataTreeNode = require('./data-tree-node');


class DataTree {
  constructor() {
    this.root = new DataTreeNode();
  }
}

module.exports = DataTree;
