
'use strict';


class DataTreeNode {
  constructor(name, data, nodes = []) {
    this.name = name;
    this.data = data;
    this.nodes = nodes;
  }
  
  addNode(node) {
    this.nodes.push(node);
  }
}

module.exports = DataTreeNode;
