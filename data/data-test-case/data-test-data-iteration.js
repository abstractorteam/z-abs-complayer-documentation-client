
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataTestDataIteration extends DataTableKeyValue {
  constructor(phase, type, key, value, description) {
    super([
      ['phase', (rowData) => {return 'exec'}],
      ['type', (rowData) => {
        if(rowData.phase === 'pre') {
          return 'cond';
        }
        else if(rowData.phase === 'exec') {
          return 'orig';
        }
        else if(rowData.phase === 'post') {
          return 'cond';
        }
      }]
    ],
    [
      ['phase', (rowData) => {
        return this.getAllowedPhase(rowData);
      }],
      ['type', (rowData) => {
        return this.getAllowedType(rowData);
      }]
    ]);
    this.phase = phase;
    this.type = type;
    this.key = key;
    this.value = value;
    this.description = description;
  }
       
  getAllowedPhase(rowData) {
    switch(rowData.phase) {
      case 'pre':
      case 'exec':
      case 'post':
        return DataTableKeyValue.ALLOWED_OK;
      default:
        return DataTableKeyValue.ALLOWED_NOK_VALUE;
    }
  }
    
  getAllowedType(rowData) {
    return DataTableKeyValue.ALLOWED_OK;
  }
}

module.exports = DataTestDataIteration;
