
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataActor extends DataTableKeyValue {
  constructor(name, node, type, phase, execution, src, dst, srv, testData, verification) {
    super([
      ['phase', (rowData) => {return this._defaultPhase(rowData);}],
      ['execution', (rowData) => {return this._defaultExecution(rowData);}]
    ],
    [
      ['type', (rowData) => {
        switch(rowData.type) {
          case 'orig':
          case 'term':
          case 'proxy':
          case 'sut':
          case 'local':
            return DataTableKeyValue.ALLOWED_OK;
          case 'cond':
            switch(rowData.phase) {
              case '':
              case 'pre, post':
                return DataTableKeyValue.ALLOWED_OK;
              default:
                return DataTableKeyValue.ALLOWED_NOK_COMBO;
            }
          default:
            return DataTableKeyValue.ALLOWED_NOK_VALUE;
        }
      }],
      ['phase', (rowData) => {
        switch(rowData.phase) {
          case 'pre':
          case 'exec':
          case 'post':
            if('cond' === rowData.type) {
              return DataTableKeyValue.ALLOWED_NOK_COMBO;
            }
            else {
              return DataTableKeyValue.ALLOWED_OK;
            }
          case 'pre, post':
          case '':
            if('cond' === rowData.type) {
              return DataTableKeyValue.ALLOWED_OK;
            }
            else {
              return DataTableKeyValue.ALLOWED_NOK_COMBO;
            }
          default:
            return DataTableKeyValue.ALLOWED_NOK_VALUE;
        }
      }],
      ['execution', (rowData) => {
        switch(rowData.execution) {
          case 'serial':
          case 'parallel':
            return DataTableKeyValue.ALLOWED_OK;
          default:
            return DataTableKeyValue.ALLOWED_NOK_VALUE;
        }
      }],
      ['src', (rowData) => {return this._allowedAddressNames(rowData.src);}],
      ['dst', (rowData) => {return this._allowedAddressNames(rowData.dst);}],
      ['srv', (rowData) => {return this._allowedAddressNames(rowData.srv);}],
      ['testData', (rowData) => {return this._allowedTestData(rowData);}],
      ['verification', (rowData) => {return this._allowedVerification(rowData);}]
    ]);
    this.name = name;
    this.node = node;
    this.type = type;
    this.phase = phase;
    this.execution = execution;
    this.src = src;
    this.dst = dst;
    this.srv = srv;
    this.testData = testData;
    this.verification = verification;
    this._phaseDefault = null;
    this._phaseExecution = null;
  }
  
  getValue(heading, rowData) {
    if('name' !== heading) {
      return super.getValue(heading, rowData);
    }
    else {
      let value = super.getValue(heading, rowData);
      let actorPaths = value.value.split('.');
      value.link = `/actor-editor/${actorPaths.join('/')}.js`
      return value;
    } 
  }
  
  _defaultPhase(rowData) {
    if('cond' === rowData.type) {
      this._phaseDefault = 'pre, post';
      return 'pre, post';
    }
    else {
      this._phaseDefault = 'exec';
      return 'exec';
    }
  }
  
  _defaultExecution(rowData) {
    if('orig' === rowData.type || 'cond' === rowData.type || 'local' === rowData.type) {
      this._phaseExecution = 'serial';
      return 'serial';
    }
    else {
      this._phaseExecution = 'parallel';
      return 'parallel';
    }
  }
  
  _allowedTestData(rowData) {
    if(undefined === rowData.testData || '' === rowData.testData) {
      return DataTableKeyValue.ALLOWED_OK;
    }
    else {
      try {
        let testDataActors = JSON.parse(rowData.testData);
        if(Array.isArray(testDataActors) && testDataActors.length >= 1) {
          let formatOk = true;
          testDataActors.forEach((testDataActor) => {
            if(Array.isArray(testDataActor)) {
              if(2 !== testDataActor.length && 3 !== testDataActor.length) {
                formatOk = false;
              }
            }
            else {
              formatOk = false;
            }
          });
          if(formatOk) {
            return DataTableKeyValue.ALLOWED_OK;
          }
          else {
            return DataTableKeyValue.ALLOWED_NOK_VALUE;
          }
        }
      }
      catch(err) {
      }
      return DataTableKeyValue.ALLOWED_NOK_VALUE;
    }
  }
  
  _allowedVerification(rowData) {
    if(undefined === rowData.verification || '' === rowData.verification) {
      return DataTableKeyValue.ALLOWED_OK;
    }
    else {
      try {
        let verificationActors = JSON.parse(rowData.verification);
        if(Array.isArray(verificationActors) && verificationActors.length >= 1) {
          let formatOk = true;
          verificationActors.forEach((verificationActor) => {
            if(Array.isArray(verificationActor)) {
              if(2 !== verificationActor.length && 3 !== verificationActor.length) {
                formatOk = false;
              }
            }
            else {
              formatOk = false;
            }
          });
          if(formatOk) {
            return DataTableKeyValue.ALLOWED_OK;
          }
          else {
            return DataTableKeyValue.ALLOWED_NOK_VALUE;
          }
        }
      }
      catch(err) {
      }
      return DataTableKeyValue.ALLOWED_NOK_VALUE;
    }
  }

  _allowedAddressName(addressName) {
    if(typeof addressName === 'string') {
      if('' === addressName) {
        return DataTableKeyValue.ALLOWED_OK;
      }
      else {
        try {
          const testDataAddressNames = JSON.parse(addressName);
          if(Array.isArray(testDataAddressNames) && testDataAddressNames.length >= 1) {
            for(let i = 0; i < testDataAddressNames.length; ++i) {
              if('string' !== typeof testDataAddressNames[i]) {
                return DataTableKeyValue.ALLOWED_NOK_VALUE; 
              }
              const notAllowedSigns = testDataAddressNames[i].replace(/\w/g, '');
              if(0 !== notAllowedSigns.length) {
                return DataTableKeyValue.ALLOWED_NOK_VALUE;
              }    
            }
            return DataTableKeyValue.ALLOWED_OK;
          }
          else if(typeof testDataAddressNames === 'object') {
            const obj = testDataAddressNames;
            if(undefined !== obj.host && '' !== obj.host) {
              // TODO: Verify correct host name.
              return DataTableKeyValue.ALLOWED_OK;
            }
            else if(undefined !== obj.uri && '' !== obj.uri) {
              // TODO: Verify correct uri.
              return DataTableKeyValue.ALLOWED_OK;
            }
            else {
              return DataTableKeyValue.ALLOWED_NOK_VALUE;
            }
          }
        }
        catch(err) {
          const notAllowedSigns = addressName.replace(/\w/g, '');
          if(0 === notAllowedSigns.length) {
            return DataTableKeyValue.ALLOWED_OK;
          }
        }
        return DataTableKeyValue.ALLOWED_NOK_VALUE;
      }
    }
    else {
      return DataTableKeyValue.ALLOWED_NOK_VALUE;
    }
  }
  
  _allowedAddressNames(addressName) {
    if(undefined === addressName) {
      return DataTableKeyValue.ALLOWED_OK;
    }
    else if(Array.isArray(addressName)) {
      return DataTableKeyValue.ALLOWED_OK; // TODO: 
    }
    else {
      return this._allowedAddressName(addressName);
    }
  }
}

module.exports = DataActor;
