
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataTestCaseSettings extends DataTableKeyValue {
  constructor(preResult, execResult, postResult) {
    super([
      ['preResult', (rowData) => {return 'success'}],
      ['execResult', (rowData) => {return 'success'}],
      ['postResult', (rowData) => {return 'success'}]
    ],
    [
      ['preResult', (rowData) => {
        return this.getAllowedResult(rowData.preResult);
      }],
      ['execResult', (rowData) => {
        return this.getAllowedResult(rowData.execResult);
      }],
      ['postResult', (rowData) => {
        return this.getAllowedResult(rowData.postResult);
      }]
    ]);
    this.preResult = preResult;
    this.execResult = execResult;
    this.postResult = postResult;
  }
  
  getAllowedResult(resultId) {
    switch(resultId) {
      case 'none':
      case 'n/a':
      case 'n_impl':
      case 'success':
      case 'n_exec':
      case 'interrupt':
      case 'failure':
      case 'error':
      case 'timeout':
      case 'reflection':
        return DataTableKeyValue.ALLOWED_OK;
      default:
        return DataTableKeyValue.ALLOWED_NOK_VALUE;
    }
  }
}

module.exports = DataTestCaseSettings;
