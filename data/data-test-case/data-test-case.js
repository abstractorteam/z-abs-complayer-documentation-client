
'use strict';


class DataTestCase {
  constructor(settings = [], actors = [], testDataTestCases = [], testDataIteration = [], verificationTestCases = []) {
    this.settings = settings;
    this.actors = actors;
    this.testDataTestCases = testDataTestCases;
    this.testDataIteration = testDataIteration;
    this.verificationTestCases = verificationTestCases;
  }
  
  addActor(actor) {
    this.actors.push(actor);
  }
}

module.exports = DataTestCase;
