
'use strict';


class DataHtmlLogStop {
  constructor(result, duration) {
    this.type = 'log_end';
    this.result = result;
    this.duration = duration;
  }
}

module.exports = DataHtmlLogStop;
