
'use strict';


class DataHtmlButton {
  constructor(glyphicon, text) {
    this.type = 'button';
    this.glyphicon = glyphicon;
    this.text = text;
  }
}

module.exports = DataHtmlButton;
