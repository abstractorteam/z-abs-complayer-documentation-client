
'use strict';


class DataHtmlLogStart {
  constructor(date) {
    this.type = 'log_start';
    this.date = date;
  }
}

module.exports = DataHtmlLogStart;
