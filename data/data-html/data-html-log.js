
'use strict';


class DataHtmlLog {
  constructor(logType, date, actor, log, fileName) {
    this.type = 'log';
    this.logType = logType;
    this.date = date;
    this.actor = actor;
    this.log = log;
    this.fileName = fileName;
  }
}

module.exports = DataHtmlLog;
