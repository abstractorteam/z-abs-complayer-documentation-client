
'use strict';


class TestSuite {
  constructor(abstractions = []) {
    this.abstractions = abstractions;
  }
  
  addAbstraction(abstraction) {
    this.abstractions.push(abstraction);
  }
}

module.exports = TestSuite;
