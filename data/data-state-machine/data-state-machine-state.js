
'use strict';


class DataStateMachineState {
  constructor(name, comment) {
    this.name = name;
    this.xIndex = -1;
    this.yIndex = -1;
    this.in = 0;
    this.out = 0;
    this.comment = comment;
  }
  
  addIndex(xIndex, yIndex) {
    this.xIndex = xIndex;
    this.yIndex = yIndex;
  }
  
  addIn() {
    ++this.in;
  }
  
  addOut() {
    ++this.out;
  }
}

module.exports = DataStateMachineState;
