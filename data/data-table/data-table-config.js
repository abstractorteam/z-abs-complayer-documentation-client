
'use strict';

const DataConfig = require('../data-config');


class DataConfigTable extends DataConfig {
  constructor() {
    super();
    this.classHeading = '';
  }
  
  addConfig(name, value) {
    if(undefined !== Reflect.get(this, name)) {
      Reflect.set(this, name, value);
    }
  }
}

module.exports = DataConfigTable;
