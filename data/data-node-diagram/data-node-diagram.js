
'use strict';

const DataNodeDiagramConfig = require('./data-node-diagram-config');


class DataNodeDiagram {
  constructor(title = '', nodeRows = [], connections = [], groups = [], xml = null) {
    this.title = title;
    this.nodeRows = nodeRows;
    this.connections = connections;
    this.groups = groups;
    this.dataNodeDiagramConfig = new DataNodeDiagramConfig();
    this.xml = xml;
  }
  
  setTitle(title) {
    this.title = title;    
  }
  
  addNodeRow() {
    this.nodeRows.push([]);
  }
  
  addNode(node) {
    this.nodeRows[this.nodeRows.length - 1].push(node);
    node.addIndex(this.nodeRows[this.nodeRows.length - 1].length -1, this.nodeRows.length - 1);
  }
  
  addConnection(connection) {
    this.connections.push(connection);
  }
  
  addGroup(group) {
    this.groups.push(group);
  }
  
  addConfig(name, value) {
    this.dataNodeDiagramConfig.addConfig(name, value);
  }
  
  addXml(xml) {
    this.xml = xml;
  }
}

module.exports = DataNodeDiagram;
