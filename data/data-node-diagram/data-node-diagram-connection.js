
'use strict';


class DataNodeDiagramConnection {
  constructor(data, type, connectionType, fromNode, toNode, fromPosition, toPosition, slope) {
    this.data = data;
    this.type = type;
    this.connectionType = connectionType;
    this.fromNodeName = fromNode ? fromNode.name : '';
    this.toNodeName = toNode ? toNode.name : '';
    this.fromPosition = fromPosition ? fromPosition : '';
    this.toPosition = toPosition ? toPosition : '';
    this.slope = slope;
  }
}

module.exports = DataNodeDiagramConnection;
