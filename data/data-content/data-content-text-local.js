
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataContentTextLocal extends DataTableKeyValue {
  constructor(name, path, mime, encoding, size, description) {
    super();
    this.name = name;
    this.path = path;
    this.mime = mime;
    this.encoding = encoding;
    this.size = size;
    this.description = description;
  }
}


module.exports = DataContentTextLocal;
