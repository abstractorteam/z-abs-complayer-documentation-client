
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataContentVideoGlobal extends DataTableKeyValue {
  constructor(name, path, mime, size, width, height, description) {
    super();
    this.name = name;
    this.path = path;
    this.mime = mime;
    this.size = size;
    this.width = width;
    this.height= height;
    this.description = description;
  }
}

module.exports = DataContentVideoGlobal;
