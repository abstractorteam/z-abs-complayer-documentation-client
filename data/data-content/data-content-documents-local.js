
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataContentDocumentsLocal extends DataTableKeyValue {
  constructor(name, path, mime, size, description) {
    super();
    this.name = name;
    this.path = path;
    this.mime = mime;
    this.size = size;
    this.description = description;
  }
}

module.exports = DataContentDocumentsLocal;
