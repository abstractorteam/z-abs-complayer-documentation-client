
'use strict';

const DataTableKeyValue = require('../data-table-key-value');


class DataSystemUnderTestAddresses extends DataTableKeyValue {
  constructor(name, sut, network, direction) {
    super();
    this.name = name;
    this.sut = sut;
    this.network = network;
    this.direction = direction;
  }
}

module.exports = DataSystemUnderTestAddresses;
