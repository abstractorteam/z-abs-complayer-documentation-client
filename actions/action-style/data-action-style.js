
'use strict';

import DataAction from 'z-abs-corelayer-client/communication/data-action';
import { ActionStyleGet } from './action-style';


export class DataActionStyleGet extends DataAction {
  constructor() {
    super();
    this.addRequest(new ActionStyleGet());
  }
}