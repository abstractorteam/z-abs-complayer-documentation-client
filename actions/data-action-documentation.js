
'use strict';

import DataAction from 'z-abs-corelayer-client/communication/data-action';
import DataActionMulti from 'z-abs-corelayer-client/communication/data-action-multi';
import { ActionDocumentationReferencesGet, ActionDocumentationGet, ActionDocumentationAdd, ActionDocumentationUpdate, ActionDocumentationNavigationsUpdate, ActionDocumentationLocalNoteGet, ActionDocumentationLocalNoteAdd, ActionDocumentationLocalNoteUpdate, ActionDocumentationLocalNoteDelete } from './action-documentation';


export class DataActionDocumentationGet extends DataAction {
  constructor(appName, documentGroup, documentName, embed, preview) {
    super();
    this.addRequest(new ActionDocumentationGet(documentName, embed, preview), appName, documentGroup, documentName, embed);
  }
}

export class DataActionDocumentationAdd extends DataAction {
  constructor(documentGroup, documentName, documentData) {
    super();
    this.addRequest(new ActionDocumentationAdd(), documentGroup, documentName, documentData);
  }
}

export class DataActionDocumentationUpdate extends DataAction {
  constructor(documentGroup, documentName, documentData) {
    super();
    this.addRequest(new ActionDocumentationUpdate(), documentGroup, documentName, documentData);
  }
}

export class DataActionDocumentationNavigationsUpdate extends DataAction {
  constructor(appName, documentGroup, navigations) {
    super();
    this.addRequest(new ActionDocumentationNavigationsUpdate(), appName, documentGroup, navigations);
  }
}

export class DataActionDocumentationLocalNoteGet extends DataAction {
  constructor(guid, path, preview) {
    super();
    this.addRequest(new ActionDocumentationLocalNoteGet(preview), guid, path);
  }
}

export class DataActionDocumentationLocalNotesAndEmbededGet extends DataAction {
  constructor(localNoteGuids, localNotePath, localNotePreview, embedAppName, embedDocumentGroup, embedDocumentName, embedEmbed, embedPreview) {
    super();
    this.addRequest(new ActionDocumentationLocalNoteGet(localNoteGuids, localNotePreview), localNoteGuids, localNotePath);
    this.addRequest(new ActionDocumentationGet(embedDocumentName, embedEmbed, embedPreview), embedAppName, embedDocumentGroup, embedDocumentName, embedEmbed);
  }
}

export class DataActionDocumentationLocalNoteAdd extends DataAction {
  constructor(guid, path, document) {
    super();
    this.addRequest(new ActionDocumentationLocalNoteAdd(guid, path), guid, path, document);
  }
}

export class DataActionDocumentationLocalNoteUpdate extends DataAction {
  constructor(guid, path, document) {
    super();
    this.addRequest(new ActionDocumentationLocalNoteUpdate(guid, path), guid, path, document);
  }
}

export class DataActionDocumentationLocalNoteDelete extends DataAction {
  constructor(guid, path) {
    super();
    this.addRequest(new ActionDocumentationLocalNoteDelete(guid), guid, path);
  }
}

export class DataActionDocumentationReferencesGet extends DataAction {
  constructor(name) {
    super();
    this.addRequest(new ActionDocumentationReferencesGet(), name);
  }
}
