
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';
import ReactDOM from 'react-dom';


export default class ComponentImageImage extends ReactComponentBase {
  constructor(props) {
    super(props);
  }

  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  render() {
    const keys = Reflect.ownKeys(this.props.data);
    const style = {};
    keys.forEach((key) => {
      if('src' !== key) {
        Reflect.set(style, key, Reflect.get(this.props.data, key));
      }
    });
    return (
      <img className="markup_image" src={this.props.data.src} style={style}
        onClick={(e) => {
        }}
      />
    );
  }
}
