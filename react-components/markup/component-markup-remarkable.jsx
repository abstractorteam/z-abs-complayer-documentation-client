
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import { Remarkable, utils } from 'remarkable';
import React from 'react';


class ComponetMarkupRemarkable extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.md = new Remarkable('full', {
      breaks: true,
      html: true
    });
    this.md.renderer.rules.paragraph_open = this._paragraph_open.bind(this);
    this.md.renderer.rules.heading_open = this._heading_open.bind(this);
  }
  
  _paragraph_open(tokens, idx) {
    if(tokens[idx].lines && tokens[idx].level === 0 && this.props.dataLineStart) {
      const start = this.props.dataLineStart + tokens[idx].lines[0];
      return `<p class="data-line" class="data-line" data-line-start="${start}" data-line-stop="${start}" data-line-object-index="0">`;
    }
    return tokens[idx].tight ? '' : '<p>';
  }

  _heading_open(tokens, idx) {
    if(tokens[idx].lines && tokens[idx].level === 0 && this.props.dataLineStart) {
      const start = this.props.dataLineStart + tokens[idx].lines[0];
      return `<h${tokens[idx].hLevel} class="data-line" data-line-start="${start}" data-line-stop="${start}" data-line-object-index="0">`;
    }
    return `<h${tokens[idx].hLevel}>`;
  }

  _link_open(tokens, idx, options) {
    const href = tokens[idx].href;
    const index = href.indexOf(':::');
    const text = -1 !== index ? href.substring(0, index) : href;
    const type = -1 !== index ? href.substring(index + 3) : 'link';
    if(this.props.preview) {
      const classJsType = 'link' === type ? '' : ` doc_nav_${type}`;
      return `<a class="doc_nav_preview${classJsType}">`;
    }
    else {
      const classAndClassJsType = 'link' === type ? '' : `class="doc_nav_${type}"`;
      const title = tokens[idx].title ? (' title="' + utils.escapeHtml(utils.replaceEntities(tokens[idx].title)) + '"') : '';
      const target = options.linkTarget ? (' target="' + options.linkTarget + '"') : '';
      return `<a ${classAndClassJsType} href="` + utils.escapeHtml(text) + '"' + title + target + '>';
    }
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  render() {
    if(undefined === this.props.value) {
      return null;
    }
    const value = this.props.value;
    if(0 === value.length) {
      return null;
    }
    const link_openOriginal = this.md.renderer.rules.link_open;
    this.md.renderer.rules.link_open = this._link_open.bind(this);
    let rawMarkup = this.md.render(value);
    this.md.renderer.rules.link_open = link_openOriginal;
    if(this.props.inner) {
      rawMarkup = rawMarkup.substring(3, rawMarkup.length - 5);
    }
    return (
      <span dangerouslySetInnerHTML={{__html: rawMarkup}} />
    );
  }
}


module.exports = ComponetMarkupRemarkable;
