
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import Link from 'z-abs-complayer-router-client/project/client/react-component/link';
import React from 'react';


export default class ComponentImageFlowchart extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.BIAS = new Map([
      ['decision',  {x:30, y:3,  textHX:0, textHY:-5, textVX:20, textVY:-5}],
      ['action',    {x:20,  y:-5, textHX:0, textHY:-5, textVX:0,  textVY:0}],
      ['start',     {x:0,  y:-5, textHX:0, textHY:-5, textVX:20, textVY:0}],
      ['end',       {x:0,  y:-5, textHX:0, textHY:-5, textVX:20, textVY:0}],
      ['connector-end', {x:0,  y:-10,  textHX:0, textHY:-5, textVX:0,  textVY:0}],
      ['connector-start', {x:0,  y:-10,  textHX:0, textHY:-5, textVX:0,  textVY:0}]
    ]);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  renderTitle(title, x) {
    if(0 !== title) {
      return (
        <text x={x} y={ComponentImageFlowchart.TITLE_TOP} textAnchor="middle" style={{fontSize:ComponentImageFlowchart.TITLE_FONT_SIZE,fontWeight:'bold'}}>
          {title}
        </text>  
      );
    }
  }
  
  calculatePoint(position, block, titleHeight) {
    const left = ComponentImageFlowchart.WIDTH_BIAS + block.xIndex * (ComponentImageFlowchart.WIDTH_BETWEEN_BLOCKS + ComponentImageFlowchart.BLOCK_WIDTH);
    const top = titleHeight + ComponentImageFlowchart.HEIGHT_BIAS + block.yIndex * (ComponentImageFlowchart.BLOCK_HEIGHT + ComponentImageFlowchart.HEIGHT_BETWEEN_BLOCKS);
    const bias = this.BIAS.get(block.type);
    if(ComponentImageFlowchart.RIGHT === position) {
      return {
        x: left + ComponentImageFlowchart.POSITION_X[position] + bias.x,
        y: top + ComponentImageFlowchart.POSITION_Y[position],
        xO: left + ComponentImageFlowchart.POSITION_X[position],
        yO: top + ComponentImageFlowchart.POSITION_Y[position]
      }
    }
    else if(ComponentImageFlowchart.LEFT === position) {
      return {
        x: left + ComponentImageFlowchart.POSITION_X[position] - bias.x,
        y: top + ComponentImageFlowchart.POSITION_Y[position],
        xO: left + ComponentImageFlowchart.POSITION_X[position],
        yO: top + ComponentImageFlowchart.POSITION_Y[position]
      }
    }
    else if(ComponentImageFlowchart.TOP === position) {
      return {
        x: left + ComponentImageFlowchart.POSITION_X[position],
        y: top + ComponentImageFlowchart.POSITION_Y[position] - bias.y,
        xO: left + ComponentImageFlowchart.POSITION_X[position],
        yO: top + ComponentImageFlowchart.POSITION_Y[position]
      }
    }
    else if(ComponentImageFlowchart.BOTTOM === position) {
      return {
        x: left + ComponentImageFlowchart.POSITION_X[position],
        y: top + ComponentImageFlowchart.POSITION_Y[position] + bias.y,
        xO: left + ComponentImageFlowchart.POSITION_X[position],
        yO: top + ComponentImageFlowchart.POSITION_Y[position]
      }
    }
    else {
      return {
        x: left + ComponentImageFlowchart.POSITION_X[position],
        y: top + ComponentImageFlowchart.POSITION_Y[position],
        xO: left + ComponentImageFlowchart.POSITION_X[position],
        yO: top + ComponentImageFlowchart.POSITION_Y[position]
      }
    }
  }
  
  calculatePosition(fromBlock, toBlock) {
    const position = {
      from: -1,
      to: -1,
      direction: -1
    };
    const xDiff = fromBlock.xIndex - toBlock.xIndex;
    const yDiff = fromBlock.yIndex - toBlock.yIndex;
    if(xDiff < 0) {
      if(yDiff < 0) {
        position.from = ComponentImageFlowchart.RIGHT;
        position.to = ComponentImageFlowchart.TOP;
        position.direction = ComponentImageFlowchart.SOUTH;
      }
      else if(yDiff > 0) {
        position.from = ComponentImageFlowchart.RIGHT;
        position.to = ComponentImageFlowchart.BOTTOM;
        position.direction = ComponentImageFlowchart.NORTH;
      }
      else {
        position.from = ComponentImageFlowchart.RIGHT;
        position.to = ComponentImageFlowchart.LEFT;
        position.direction = ComponentImageFlowchart.WEST;
      }
    }
    else if(xDiff > 0) {
      if(yDiff < 0) {
        position.from = ComponentImageFlowchart.LEFT;
        position.to = ComponentImageFlowchart.TOP;
        position.direction = ComponentImageFlowchart.SOUTH;
      }
      else if(yDiff > 0) {
        position.from = ComponentImageFlowchart.LEFT;
        position.to = ComponentImageFlowchart.BOTTOM;
        position.direction = ComponentImageFlowchart.NORTH;
      }
      else {
        position.from = ComponentImageFlowchart.TOP;
        position.to = ComponentImageFlowchart.BOTTOM;
        position.direction = ComponentImageFlowchart.EAST;
      }
    }
    else {
      if(yDiff < 0) {
        position.from = ComponentImageFlowchart.BOTTOM;
        position.to = ComponentImageFlowchart.TOP;
        position.direction = ComponentImageFlowchart.SOUTH;
      }
      else if(yDiff > 0) {
        position.from = ComponentImageFlowchart.TOP;
        position.to = ComponentImageFlowchart.BOTTOM;
        position.direction = ComponentImageFlowchart.NORTH;
      }
      else {
        position.from = ComponentImageFlowchart.TOP;
        position.to = ComponentImageFlowchart.BOTTOM;
        position.direction = ComponentImageFlowchart.NORTH;
      }
    }
    return position;
  }
  
  calculateTextPoint(outPoint, inPoint) {
    return {
      x: (outPoint.xO + inPoint.xO) / 2,
      y: (outPoint.yO + inPoint.yO) / 2
    };
  }
  
  renderArrow(x, y, direction) {
    if(ComponentImageFlowchart.SOUTH === direction) {
      return (<polygon className="" points={`${x+4},${y-8}, ${x-4},${y-8},${x},${y}`} />);
    }
    else if(ComponentImageFlowchart.WEST === direction) {
      return (<polygon className="" points={`${x+8},${y+4}, ${x+8},${y-4},${x},${y}`} />);
    }
    else if(ComponentImageFlowchart.EAST === direction) {
      return (<polygon className="" points={`${x-8},${y+4}, ${x-8},${y-4},${x},${y}`} />);
    }      
    else if(ComponentImageFlowchart.NORTH === direction) {
      return (<polygon className="" points={`${x+4},${y+8}, ${x-4},${y+8},${x},${y}`} />);
    }
  }
  
  renderConnection(connection, fromBlock, toBlock, titleHeight) {
    const position = this.calculatePosition(fromBlock, toBlock);
    const currentOut = fromBlock.currentOut++;
    const currentIn = toBlock.currentIn++;
    const outPoint = this.calculatePoint(position.from, fromBlock, titleHeight);
    const inPoint = this.calculatePoint(position.to, toBlock, titleHeight);
    let type = `flowchart_line_${connection.type}`;
    if('part' === connection.connectionType || 'part-bi-directional' === connection.connectionType) {
     type += ' seq_message_part';
    }
    const bias = this.BIAS.get(fromBlock.type);
    if(outPoint.x === inPoint.x) {
      const textPoint = this.calculateTextPoint(outPoint, inPoint);
      return (
        <g key={`key_${fromBlock.xIndex}_${fromBlock.xIndex}_${toBlock.yIndex}_${toBlock.yIndex}_${currentOut}_${currentIn}`}>
          <line className={type} x1={outPoint.x} y1={outPoint.y} x2={inPoint.x} y2= {inPoint.y} style={{strokeWidth:2}} />
          {this.renderArrow(inPoint.x, inPoint.y, position.direction)}
          <text x={textPoint.x + bias.textVX} y={textPoint.y + bias.textVY} style={{fontSize:'12px',fontWeight:'bold',textAnchor:'middle'}}>
            {connection.data}
          </text>
        </g>
      );
    }
    else {
      const textPoint = this.calculateTextPoint(outPoint, {xO:inPoint.xO, yO:outPoint.yO});
      return (
        <g key={`key_${fromBlock.xIndex}_${fromBlock.xIndex}_${toBlock.yIndex}_${toBlock.yIndex}_${currentOut}_${currentIn}`}>
          <line className={type} x1={outPoint.x} y1={outPoint.y} x2={inPoint.x} y2= {outPoint.y} style={{strokeWidth:2}} />
          <line className={type} x1={inPoint.x} y1={outPoint.y} x2={inPoint.x} y2= {inPoint.y} style={{strokeWidth:2}} />
          {this.renderArrow(inPoint.x, inPoint.y, position.direction)}
          <text x={textPoint.x + bias.textHX} y={textPoint.y + bias.textHY} style={{fontSize:'12px',fontWeight:'bold',textAnchor:'middle'}}>
            {connection.data}
          </text>
        </g>
      );
    }
  }
  
  renderConnections(connections, blockMap, titleHeight) {
    return connections.map((connection) => {
      if('' !== connection.fromBlockName && '' !== connection.toBlockName) {
        const fromBlock = blockMap.get(connection.fromBlockName);
        const toBlock = blockMap.get(connection.toBlockName);
        return this.renderConnection(connection, fromBlock, toBlock, titleHeight);
      }
    });
  }
  
  renderStartEnd(block, titleHeight, text) {
    const bias = this.BIAS.get(block.type);
    const left = ComponentImageFlowchart.WIDTH_BIAS + block.xIndex * (ComponentImageFlowchart.WIDTH_BETWEEN_BLOCKS + ComponentImageFlowchart.BLOCK_WIDTH);
    const top = titleHeight + ComponentImageFlowchart.HEIGHT_BIAS + block.yIndex * (ComponentImageFlowchart.BLOCK_HEIGHT + ComponentImageFlowchart.HEIGHT_BETWEEN_BLOCKS);
    return (
      <g key={`block_${block.xIndex}_${block.yIndex}`}>
        <rect className="flowchart_block_start_end" x={left - bias.x} y={top - bias.y} rx={ComponentImageFlowchart.BLOCK_HEIGHT_HALF} ry={ComponentImageFlowchart.BLOCK_HEIGHT_HALF} width={ComponentImageFlowchart.BLOCK_WIDTH + bias.x + bias.x} height={ComponentImageFlowchart.BLOCK_HEIGHT + bias.y + bias.y} style={block.css ? {fill: block.css} : {}} />
        <text x={left + ComponentImageFlowchart.BLOCK_WIDTH_HALF} y={top + ComponentImageFlowchart.BLOCK_HEIGHT_HALF + 4} style={{fontSize:'10px',fontWeight:'bold',textAnchor:'middle'}}>
          {block.data ? block.data : text}
        </text>
      </g>
    );
  }
  
  renderDecision(block, titleHeight) {
    const bias = this.BIAS.get(block.type);
    const left = ComponentImageFlowchart.WIDTH_BIAS + block.xIndex * (ComponentImageFlowchart.WIDTH_BETWEEN_BLOCKS + ComponentImageFlowchart.BLOCK_WIDTH);
    const top = titleHeight + ComponentImageFlowchart.HEIGHT_BIAS + block.yIndex * (ComponentImageFlowchart.BLOCK_HEIGHT + ComponentImageFlowchart.HEIGHT_BETWEEN_BLOCKS);
    return (
      <g key={`block_${block.xIndex}_${block.yIndex}`}>
        <polygon className="flowchart_block_decision" points={`${left - bias.x},${top + ComponentImageFlowchart.BLOCK_HEIGHT_HALF}, ${left + ComponentImageFlowchart.BLOCK_WIDTH_HALF},${top - bias.y} ${left + bias.x + ComponentImageFlowchart.BLOCK_WIDTH},${top + ComponentImageFlowchart.BLOCK_HEIGHT_HALF}, ${left + ComponentImageFlowchart.BLOCK_WIDTH_HALF},${top + bias.y + ComponentImageFlowchart.BLOCK_HEIGHT}`} style={block.css ? {fill: block.css} : {}} />
        <text x={left + ComponentImageFlowchart.BLOCK_WIDTH_HALF} y={top + ComponentImageFlowchart.BLOCK_HEIGHT_HALF + 4} style={{fontSize:'10px',fontWeight:'bold',textAnchor:'middle'}}>
          {block.data ? block.data : ''}
        </text>
      </g>
    );
  }
  
  renderActionBlockInnerText(left, top, block, inner = false) {
    return (
      <text className={!inner ? 'flowchart_block_action_text' : ''} x={left + ComponentImageFlowchart.BLOCK_WIDTH_HALF} y={top + ComponentImageFlowchart.BLOCK_HEIGHT_HALF + 4}>
        {block.data ? block.data : ''}
      </text>
    );
  }
  
  renderActionBlockText(left, top, block) {
    if(undefined === block.link || '' === block.link) {
      return this.renderActionBlockInnerText(left, top, block);
    }
    else {
      return (
        <Link className="flowchart_block_action_text" href={block.link}>
          {this.renderActionBlockInnerText(left, top, block, true)}
        </Link>
      );
    }
  }
  
  renderActionBlock(block, titleHeight) {
    const bias = this.BIAS.get(block.type);
    const left = ComponentImageFlowchart.WIDTH_BIAS + block.xIndex * (ComponentImageFlowchart.WIDTH_BETWEEN_BLOCKS + ComponentImageFlowchart.BLOCK_WIDTH);
    const top = titleHeight + ComponentImageFlowchart.HEIGHT_BIAS + block.yIndex * (ComponentImageFlowchart.BLOCK_HEIGHT + ComponentImageFlowchart.HEIGHT_BETWEEN_BLOCKS);
    return (
      <g key={`block_${block.xIndex}_${block.yIndex}`}>
        <rect className="flowchart_block_action" x={left - bias.x} y={top - bias.y} rx="2" ry="2" width={ComponentImageFlowchart.BLOCK_WIDTH + bias.x + bias.x} height={ComponentImageFlowchart.BLOCK_HEIGHT + bias.y + bias.y} style={block.css ? {fill: block.css} : {}} />
        {this.renderActionBlockText(left, top, block)}
      </g>
    );
  }
  
  renderConnector(block, titleHeight) {
    const bias = this.BIAS.get(block.type);
    const left = ComponentImageFlowchart.WIDTH_BIAS + block.xIndex * (ComponentImageFlowchart.WIDTH_BETWEEN_BLOCKS + ComponentImageFlowchart.BLOCK_WIDTH);
    const top = titleHeight + ComponentImageFlowchart.HEIGHT_BIAS + block.yIndex * (ComponentImageFlowchart.BLOCK_HEIGHT + ComponentImageFlowchart.HEIGHT_BETWEEN_BLOCKS);
    return (
      <g key={`block_${block.xIndex}_${block.yIndex}`}>
        <circle className="flowchart_block_connector" cx={left + ComponentImageFlowchart.BLOCK_WIDTH_HALF} cy={top + ComponentImageFlowchart.BLOCK_HEIGHT_HALF} r={ComponentImageFlowchart.BLOCK_HEIGHT_HALF + bias.y} style={block.css ? {fill: block.css} : {}} />
        <text x={left + ComponentImageFlowchart.BLOCK_WIDTH_HALF} y={top + ComponentImageFlowchart.BLOCK_HEIGHT_HALF + 4} style={{fontSize:'10px',fontWeight:'bold',textAnchor:'middle'}}>
          {block.data ? block.data : ''}
        </text>
      </g>
    );
  }
      
  renderBlock(block, titleHeight) {
    if('' === block.name) {
      return null;
    }
    if(undefined !== block.type) {
      switch(block.type.toLowerCase()) {
        case 'decision':
          return this.renderDecision(block, titleHeight);
        case 'action':
          return this.renderActionBlock(block, titleHeight);
        case 'start':
          return this.renderStartEnd(block, titleHeight, 'Start');
        case 'end':
          return this.renderStartEnd(block, titleHeight, 'End');
        case 'connector-end':
          return this.renderConnector(block, titleHeight);
        case 'connector-start':
          return this.renderConnector(block, titleHeight);
      }
    }
    else {
      return this.renderActionBlock(block, titleHeight);
    }
  }
  
  renderBlocks(blockRows, titleHeight) {
    const svgBlocks = blockRows.map((blocks, yIndex) => {
      return blocks.map((block, xIndex) => {
        return this.renderBlock(block, titleHeight);
      });
    });
    return (
      <g>
        {svgBlocks}
      </g>
    );
  }  

  render() {
    const title = this.props.flowchart.title;
    const blockRows = this.props.flowchart.blockRows;
    const connections = this.props.flowchart.connections;
    let columns = 0;
    const blockMap = new Map();
    blockRows.forEach((blocks, yIndex) => {
      blocks.forEach((block, xIndex) => {
        blockMap.set(block.name, {
          xIndex: xIndex,
          yIndex: yIndex,
          in: block.in,
          out: block.out,
          currentIn: 0,
          currentOut: 0,
          type: block.type
        });
      });
      if(blocks.length > columns) {
        columns = blocks.length;
      }
    });
    const titleHeight = ('' !== title && undefined !== title) ? ComponentImageFlowchart.TITLE_HEIGHT : 0;
    const blockHeight = ComponentImageFlowchart.HEIGHT_BIAS + blockRows.length * ComponentImageFlowchart.BLOCK_HEIGHT + (blockRows.length - 1) * ComponentImageFlowchart.HEIGHT_BETWEEN_BLOCKS;
    const svgHeight = titleHeight + blockHeight + ComponentImageFlowchart.HEIGHT_BIAS;
    const svgWidth = 2 * ComponentImageFlowchart.WIDTH_BIAS + columns * ComponentImageFlowchart.BLOCK_WIDTH + (columns - 1) * ComponentImageFlowchart.WIDTH_BETWEEN_BLOCKS;
    const viewBox = [0, 0, svgWidth, svgHeight].join(' ');
    return (
      <svg className="markup_flow" width={svgWidth} height={svgHeight} viewBox={viewBox}>
        {this.renderTitle(title, svgWidth / 2)}
        {this.renderBlocks(blockRows, titleHeight)}
        {this.renderConnections(connections, blockMap, titleHeight)}
      </svg>
    );
  }
}

ComponentImageFlowchart.TITLE_HEIGHT = 40;
ComponentImageFlowchart.TITLE_FONT_SIZE = 20;
ComponentImageFlowchart.TITLE_TOP = ComponentImageFlowchart.TITLE_FONT_SIZE + (ComponentImageFlowchart.TITLE_HEIGHT - ComponentImageFlowchart.TITLE_FONT_SIZE) / 2;
ComponentImageFlowchart.WIDTH_BIAS = 30;
ComponentImageFlowchart.HEIGHT_BIAS = 10;
ComponentImageFlowchart.BLOCK_HEIGHT = 40;
ComponentImageFlowchart.BLOCK_HEIGHT_HALF = ComponentImageFlowchart.BLOCK_HEIGHT / 2;
ComponentImageFlowchart.HEIGHT_BETWEEN_BLOCKS = -ComponentImageFlowchart.BLOCK_HEIGHT_HALF + 15;
ComponentImageFlowchart.BLOCK_WIDTH = 80;
ComponentImageFlowchart.BLOCK_WIDTH_HALF = ComponentImageFlowchart.BLOCK_WIDTH / 2;
ComponentImageFlowchart.WIDTH_BETWEEN_BLOCKS = ComponentImageFlowchart.BLOCK_WIDTH;

ComponentImageFlowchart.TOP = 0;
ComponentImageFlowchart.RIGHT = 1;
ComponentImageFlowchart.BOTTOM = 2;
ComponentImageFlowchart.LEFT = 3;
ComponentImageFlowchart.POSITION_X = [ComponentImageFlowchart.BLOCK_WIDTH_HALF, ComponentImageFlowchart.BLOCK_WIDTH, ComponentImageFlowchart.BLOCK_WIDTH_HALF, 0];
ComponentImageFlowchart.POSITION_Y = [0, ComponentImageFlowchart.BLOCK_HEIGHT_HALF, ComponentImageFlowchart.BLOCK_HEIGHT, ComponentImageFlowchart.BLOCK_HEIGHT_HALF];

ComponentImageFlowchart.NORTH = 0;
ComponentImageFlowchart.EAST = 1;
ComponentImageFlowchart.SOUTH = 2;
ComponentImageFlowchart.WEST = 3;

ComponentImageFlowchart.TEXT_BIAS_Y = 10;

