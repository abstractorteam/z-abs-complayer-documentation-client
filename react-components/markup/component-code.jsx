
'use strict';

import CodeMirrorEditor from 'z-abs-complayer-codemirror-client/code-mirror-editor';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ComponentCode extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  render(options) {
    return (
      <div>
        <p className="markup_code">{this.props.name}</p>
        <CodeMirrorEditor code={this.props.code} options={this.getOptions(this.props.type)} height="100%" />
      </div>
    );
  }
  
  getOptions(type) {
    let mode = 'text/plain';
    switch(type) {
      case 'css':
        mode = 'text/css';
        break;
      case 'scss':
        mode = 'text/x-scss';
        break;
      case 'less':
        mode = 'text/x-less';
        break;
      case 'jsx':
        mode = 'text/jsx';
        break;
      case 'js':
        mode = 'text/javascript';
        break;
      case 'json':
        mode = 'application/json';
        break;
      case 'html':
        mode = 'text/html';
        break;
      default:
    }
    return {
      readOnly: true,
      fixedGutter: false,
      indentUnit: 2,
      lineNumbers: true,
      matchBrackets: true,
      mode: mode,
      tabSize: 2
    };
  }
}
