
'use strict';

import ComponentBootstrapGroup from 'z-abs-complayer-bootstrap-client/group';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ComponentImageLab extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.nodeY = 30;
    this.networkWidth = 20;
    this.nodeWidth = 100;
    this.nodeWidthHalf = this.nodeWidth / 2;
    this.nodeHeight = 0;
    
    this.nodeClientX = 130;
    this.clientNetworkX = this.nodeClientX + this.nodeWidth - 4;
    this.clientAddressesX = this.clientNetworkX + this.networkWidth;
    
    this.nodeSutX = 750;
    this.sutServerNetworkX = this.nodeSutX - this.networkWidth + 4;
    this.sutServerAddressesX = this.sutServerNetworkX;
    this.sutClientNetworkX = this.nodeSutX + this.nodeWidth - 4;
    this.sutClientAddressesX = this.sutClientNetworkX + this.networkWidth;
    
    this.nodeServerX = 1370;
    this.serverNetworkX = this.nodeServerX - this.networkWidth + 4;
    this.serverAddressesX = this.serverNetworkX;
    
    this.networkBias = 36;
    this.networkExtraBias = 16;
    this.addressBias = this.networkBias / 2;
    this.cloudYBias = this.networkBias / 3;
    this.cloudXBias = this.addressBias / 2;
    
    this.virtualNetworksHeight = [];
    this.networkYs = [];
    
    this.bottom = 0;
    this.helpHeight = 30;
    
    this.key = 0;
    
    this.InterfaceRadiusOuter = 12;
    this.InterfaceRadiusInner = 10;
    this.InterfaceGuiWidthOuter = 20;
    this.InterfaceGuiWidthInner = 12;
    this.InterfaceGuiWidthOuterHalf = this.InterfaceGuiWidthOuter / 2;
    this.InterfaceGuiWidthInnerHalf = this.InterfaceGuiWidthInner / 2;
  }

  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.width, nextProps.width)
      || !this.shallowCompare(this.props.height, nextProps.height)
      || !this.shallowCompare(this.props.virtualNetworks, nextProps.virtualNetworks)
      || !this.shallowCompare(this.props.pingResults, nextProps.pingResults);
  }
  
  renderClientInterface(client, x, y) {
    if('gui' === client.type) {
      return (
        <g>
          <rect className={`${this._addressPrefixClass(client)}outer_address`} x={x - this.InterfaceGuiWidthOuterHalf} y={y - this.InterfaceGuiWidthOuterHalf} rx="1" ry="1" width={this.InterfaceGuiWidthOuter} height={this.InterfaceGuiWidthOuter} />
          <rect className={this._addressPostfixClass(client)} x={x - this.InterfaceGuiWidthInnerHalf} y={y - this.InterfaceGuiWidthInnerHalf} width={this.InterfaceGuiWidthInner} height={this.InterfaceGuiWidthInner} />
        </g>
      );
    }
    else if('ip' === client.type) {
      return (
        <g>
          <circle className="outer_address" cx={x} cy={y} r={this.InterfaceRadiusOuter} />
          <circle className={this._addressClass(client)} cx={x} cy={y} r={this.InterfaceRadiusInner} />
        </g>
      );
    }
  }
  
  renderClientPing(client, x, y) {
    const key = `client_${client.type}_${client.networkName}_${client.interfaceName}`;
    if('gui' === client.type) {
      return (
        <g key={key}>
          <rect className={`outer_address ${this._pingResultClass(client.address)}`} x={x - this.InterfaceGuiWidthOuterHalf} y={y - this.InterfaceGuiWidthOuterHalf} rx="1" ry="1" width={this.InterfaceGuiWidthOuterHalf} height={this.InterfaceGuiWidthOuter} />
        </g>
      );
    }
    else if('ip' === client.type) {
      const cutoffId = `cut-off-right-circle_${key}`
      return (
        <React.Fragment key={key}>
          <defs>
            <clipPath id={cutoffId}>
              <rect x={x - this.InterfaceRadiusOuter - 1} y={y - this.InterfaceRadiusOuter} width={this.InterfaceRadiusOuter} height={2 * this.InterfaceRadiusOuter} />
            </clipPath>
          </defs>
          <circle className={`outer_address ${this._pingResultClass(client.address)}`} cx={x} cy={y} r={this.InterfaceRadiusOuter} clipPath={`url(#${cutoffId})`} />
        </React.Fragment>
      );
    }
  }
  
  renderClientAddress(client, virtualNetworkIndex, addressIndex) {
    const y = this.networkYs[virtualNetworkIndex] + this.addressBias + (2 * this.addressBias * addressIndex);
    const popup = [['name:', client.interfaceName], ['address:', client.address], ['netmask:', client.netmask], ['network:', client.networkName], ['type:', client.type], [client.static ? 'static' : 'dynamic', '']];
    if(client.multicast) {
      popup.push(['multicast', '']);
    }
    if(client.external) {
      popup.push(['external', '']);
    }
    if(client.reduced) {
      popup.push(['reduced', '']);
    }
    if(!client.valid) {
      popup.push(['address exist:', client.validExist, client.validExist ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid address:', client.validAddress, client.validAddress ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid netmask:', client.validNetmask, client.validNetmask ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid family:', client.validFamily, client.validFamily ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid subnet:', client.validSubnet, client.validSubnet ? 'login_valid_success' : 'login_valid_failure']);
    }
    return (
      <g key={++this.key}>
        <text key={++this.key} x={this.clientAddressesX + 16} y={y + 4}>
          {`${client.interfaceName}: ${null !== client.address ? client.address : ''}`}
        </text>
        <ComponentBootstrapGroup className="pull-right" placement="left" title="Address - client" showTime={-1} text={popup}>
          {this.renderClientInterface(client, this.clientAddressesX, y)}
        </ComponentBootstrapGroup>
      </g>
    );
  }
  
  renderSutClientPing(sutClient, x, y) {
    const key = `sutClient_${sutClient.type}_${sutClient.networkName}_${sutClient.interfaceName}`;
    if('gui' === sutClient.type) {
      return (
        <g key={key}>
          <rect className={`outer_address ${this._pingResultClass(sutClient.address)}`} x={x - this.InterfaceGuiWidthOuterHalf} y={y - this.InterfaceGuiWidthOuterHalf} rx="1" ry="1" width={this.InterfaceGuiWidthOuterHalf} height={this.InterfaceGuiWidthOuter} />
        </g>
      );
    }
    else if('ip' === sutClient.type) {
      const cutoffId = `cut-off-right-circle_${key}`
      return (
        <React.Fragment key={key}>
          <defs>
            <clipPath id={cutoffId}>
              <rect x={x - this.InterfaceRadiusOuter - 1} y={y - this.InterfaceRadiusOuter} width={this.InterfaceRadiusOuter} height={2 * this.InterfaceRadiusOuter} />
            </clipPath>
          </defs>
          <circle className={`outer_address ${this._pingResultClass(sutClient.address)}`} cx={x} cy={y} r={this.InterfaceRadiusOuter} clipPath={`url(#${cutoffId})`} />
        </React.Fragment>
      );
    }
  }
  
  renderSutServerPing(sutServer, x, y) {
    const key = `sutServer${sutServer.type}_${sutServer.networkName}_${sutServer.interfaceName}`;
    if('gui' === sutServer.type) {
      return (
        <g key={key}>
          <rect className={`outer_address ${this._pingResultClass(sutServer.address)}`} x={x} y={y - this.InterfaceGuiWidthOuterHalf} rx="1" ry="1" width={this.InterfaceGuiWidthOuterHalf} height={this.InterfaceGuiWidthOuter} />
        </g>
      );
    }
    else if('ip' === sutServer.type) {
      const cutoffId = `cut-off-right-circle_${key}`
      return (
        <React.Fragment key={key}>
          <defs>
            <clipPath id={cutoffId}>
              <rect x={x} y={y - this.InterfaceRadiusOuter} width={this.InterfaceRadiusOuter + 1} height={2 * this.InterfaceRadiusOuter} />
            </clipPath>
          </defs>
          <circle className={`outer_address ${this._pingResultClass(sutServer.address)}`} cx={x} cy={y} r={this.InterfaceRadiusOuter} clipPath={`url(#${cutoffId})`} />
        </React.Fragment>
      );
    }
  }
  
  renderSutServerInterface(sutServer, x, y) {
    if('gui' === sutServer.type) {
      return (
        <g>
          <rect className={`${this._addressPrefixClass(sutServer)}outer_address`} x={x - this.InterfaceGuiWidthOuterHalf} y={y - this.InterfaceGuiWidthOuterHalf} rx="1" ry="1" width={this.InterfaceGuiWidthOuter} height={this.InterfaceGuiWidthOuter} />
          <rect className={this._addressPostfixClass(sutServer)} x={x - this.InterfaceGuiWidthInnerHalf} y={y - this.InterfaceGuiWidthInnerHalf} width={this.InterfaceGuiWidthInner} height={this.InterfaceGuiWidthInner} />
        </g>
      );
    }
    else if('ip' === sutServer.type) {
      return (
        <g>
          <circle className="outer_address" cx={x} cy={y} r={this.InterfaceRadiusOuter} />
          <circle className={this._addressClass(sutServer)} cx={x} cy={y} r={this.InterfaceRadiusInner} />
        </g>
      );
    }
  }

  renderSutServerAddress(sutServer, virtualNetworkIndex, addressIndex) {
    const y = this.networkYs[virtualNetworkIndex] + this.addressBias + (2 * this.addressBias * addressIndex);
    const popup = [['name:', sutServer.interfaceName], ['address:', sutServer.address], ['netmask:', sutServer.netmask], ['network:', sutServer.networkName], ['type:', sutServer.type], [sutServer.static ? 'static' : 'dynamic', '']];
    if(sutServer.multicast) {
      popup.push(['multicast', '']);
    }
    if(sutServer.external) {
      popup.push(['external', '']);
    }
    if(sutServer.reduced) {
      popup.push(['reduced', '']);
    }
    if(!sutServer.valid && !(sutServer.external && sutServer.validAddress && sutServer.validNetmask && sutServer.validFamily) && !(sutServer.multicast && sutServer.validAddress && sutServer.validFamily)) {
      popup.push(['address exist:', sutServer.validExist, sutServer.validExist ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid address:', sutServer.validAddress, sutServer.validAddress ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid netmask:', sutServer.validNetmask, sutServer.validNetmask ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid family:', sutServer.validFamily, sutServer.validFamily ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid subnet:', sutServer.validSubnet, sutServer.validSubnet ? 'login_valid_success' : 'login_valid_failure']);
    }
    return (
      <g key={++this.key}>
        <text key={++this.key} x={this.sutServerAddressesX - 16} y={y + 4} textAnchor="end">
          {`${sutServer.interfaceName}: ${null !== sutServer.address ? sutServer.address : ''}`}
        </text>
        <ComponentBootstrapGroup className="pull-right" placement="right" title="Address - sut server" showTime={-1} text={popup}>
          {this.renderSutServerInterface(sutServer, this.sutServerAddressesX, y)}
        </ComponentBootstrapGroup>
      </g>
    );
  }
  
  renderSutClientAddress(sutClient, virtualNetworkIndex, addressIndex) {
    const y = this.networkYs[virtualNetworkIndex] + this.addressBias + (2 * this.addressBias * addressIndex);
    const popup = [['name:', sutClient.interfaceName], ['address:', sutClient.address], ['netmask:', sutClient.netmask], ['network:', sutClient.networkName], [sutClient.static ? 'static' : 'dynamic', '']];
    if(sutClient.multicast) {
      popup.push(['multicast', '']);
    }
    if(sutClient.external) {
      popup.push(['external', '']);
    }
    if(sutClient.reduced) {
      popup.push(['reduced', '']);
    }
    if(!sutClient.valid && !(sutClient.external && sutClient.validAddress && sutClient.validNetmask && sutClient.validFamily) && !(sutClient.multicast && sutClient.validAddress && sutClient.validFamily)) {
      popup.push(['address exist:', sutClient.validExist, sutClient.validExist ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid address:', sutClient.validAddress, sutClient.validAddress ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid netmask:', sutClient.validNetmask, sutClient.validNetmask ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid family:', sutClient.validFamily, sutClient.validFamily ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid subnet:', sutClient.validSubnet, sutClient.validSubnet ? 'login_valid_success' : 'login_valid_failure']);
    }
    return (
      <g key={++this.key}>
        <text key={++this.key} x={this.sutClientAddressesX + 16} y={y + 4}>
          {`${sutClient.interfaceName}: ${null !== sutClient.address ? sutClient.address : ''}`}
        </text>
        <circle className="outer_address" cx={this.sutClientAddressesX + 2} cy={y} r={this.InterfaceRadiusOuter} />
        <ComponentBootstrapGroup className="pull-right" placement="left" title="Address - sut client" showTime={-1} text={popup}>
          <circle className={this._addressClass(sutClient)} cx={this.sutClientAddressesX + 2} cy={y} r={this.InterfaceRadiusInner} />
        </ComponentBootstrapGroup>
      </g>
    );
  }

  renderServerAddress(server, virtualNetworkIndex, addressIndex) {
    const y = this.networkYs[virtualNetworkIndex] + this.addressBias + (2 * this.addressBias * addressIndex); 
    const popup = [['name:', server.interfaceName], ['address:', server.address], ['netmask:', server.netmask], ['network:', server.networkName], [server.static ? 'static' : 'dynamic', '']];
    if(server.multicast) {
      popup.push(['multicast', '']);
    }
    if(server.external) {
      popup.push(['external', '']);
    }
    if(server.reduced) {
      popup.push(['reduced', '']);
    }
    if(!server.valid) {
      popup.push(['address exist:', server.validExist, server.validExist ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid address:', server.validAddress, server.validAddress ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid netmask:', server.validNetmask, server.validNetmask ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid family:', server.validFamily, server.validFamily ? 'login_valid_success' : 'login_valid_failure']);
      popup.push(['valid subnet:', server.validSubnet, server.validSubnet ? 'login_valid_success' : 'login_valid_failure']);
    }
    return (
      <g key={++this.key}>
        <text key={++this.key} x={this.serverAddressesX - 16} y={y + 4} textAnchor="end">
          {`${server.interfaceName}: ${null !== server.address ? server.address : ''}`}
        </text>
        <circle className="outer_address" cx={this.serverAddressesX - 2} cy={y} r={this.InterfaceRadiusOuter} />
        <ComponentBootstrapGroup className="pull-right" placement="right" title="Address - server" showTime={-1} text={popup}>
          <circle className={this._addressClass(server)} cx={this.serverAddressesX - 2} cy={y} r={this.InterfaceRadiusInner} />
        </ComponentBootstrapGroup>
      </g>
    );
  }
  
  renderServerPing(server, x, y) {
    const key = `server${server.type}_${server.networkName}_${server.interfaceName}`;
    if('gui' === server.type) {
      return (
        <g key={key}>
          <rect className={`outer_address ${this._pingResultClass(server.address)}`} x={x} y={y - this.InterfaceGuiWidthOuterHalf} rx="1" ry="1" width={this.InterfaceGuiWidthOuterHalf} height={this.InterfaceGuiWidthOuter} />
        </g>
      );
    }
    else if('ip' === server.type) {
      const cutoffId = `cut-off-right-circle_${key}`
      return (
        <React.Fragment key={key}>
          <defs>
            <clipPath id={cutoffId}>
              <rect x={x} y={y - this.InterfaceRadiusOuter} width={this.InterfaceRadiusOuter + 1} height={2 * this.InterfaceRadiusOuter} />
            </clipPath>
          </defs>
          <circle className={`outer_address ${this._pingResultClass(server.address)}`} cx={x} cy={y} r={this.InterfaceRadiusOuter} clipPath={`url(#${cutoffId})`} />
        </React.Fragment>
      );
    }
  }
  
  renderClientNetwork(virtualNetwork, index) {
    return (
      <g key={++this.key}>
        <ComponentBootstrapGroup className="pull-right" placement="left" title="Network" showTime={-1} text={this._networkPopover(virtualNetwork)}>
          <rect key={++this.key} x={this.clientNetworkX} y={this.networkYs[index]} rx="4" ry="8" width={this.networkWidth} height={this.virtualNetworksHeight[index]} style={{fill:"#f9ffff",stroke:"black",strokeWidth:2}} />
        </ComponentBootstrapGroup>
      </g>
    );
  }
  
  renderSutServerNetwork(virtualNetwork, index) {
    return (
      <g key={++this.key}>
        <ComponentBootstrapGroup className="pull-right" placement="right" title="Network" showTime={-1} text={this._networkPopover(virtualNetwork)}>
          <rect key={++this.key} x={this.sutServerNetworkX} y={this.networkYs[index]} rx="4" ry="8" width={this.networkWidth} height={this.virtualNetworksHeight[index]} style={{fill:"#f9ffff",stroke:"black",strokeWidth:2}} />
        </ComponentBootstrapGroup>
      </g>
    );
  }
  
  renderSutClientNetwork(virtualNetwork, index) {
    return (
      <g key={++this.key}>
        <ComponentBootstrapGroup className="pull-right" placement="left" title="Network" showTime={-1} text={this._networkPopover(virtualNetwork)}>
          <rect key={++this.key} x={this.sutClientNetworkX} y={this.networkYs[index]} rx="4" ry="8" width={this.networkWidth} height={this.virtualNetworksHeight[index]} style={{fill:"#f9ffff",stroke:"black",strokeWidth:2}} />
        </ComponentBootstrapGroup>
      </g>
    );
  }
  
  renderServerNetwork(virtualNetwork, index) {
    return (
      <g key={++this.key}>
        <ComponentBootstrapGroup className="pull-right" placement="right" title="Network"showTime={-1}  text={this._networkPopover(virtualNetwork)}>
          <rect key={++this.key} x={this.serverNetworkX} y={this.networkYs[index]} rx="4" ry="8" width={this.networkWidth} height={this.virtualNetworksHeight[index]} style={{fill:"#f9ffff",stroke:"black",strokeWidth:2}} />
        </ComponentBootstrapGroup>
      </g>
    );
  }
  
  renderNodeClient() {
    let radius = 10;
    return (
      <>
        <text key={++this.key} x={this.nodeClientX + this.nodeWidthHalf} y="20" textAnchor="middle" style={{fontWeight:'bold'}}>Actor Clients</text>
        <rect key={++this.key} x={this.nodeClientX} y={this.nodeY} rx={radius} ry={radius} width={this.nodeWidth} height={this.nodeHeight} style={{fill:"#f9ffff",stroke:"black",strokeWidth:2}} />
      </>
    );
  }

  renderNodeSut() {
    let radius = 10;
    return (
      <>
        <text key={++this.key} x={this.nodeSutX + this.nodeWidthHalf} y="20" textAnchor="middle" style={{fontWeight:'bold'}}>System Under Test</text>
        <rect key={++this.key} x={this.nodeSutX} y={this.nodeY} rx={radius} ry={radius} width={this.nodeWidth} height={this.nodeHeight} style={{fill:"#f9ffff",stroke:"black",strokeWidth:2}} />
      </>
    );
  }

  renderNodeServer() {
    let radius = 10;
    return (
      <>
        <text key={++this.key} x={this.nodeServerX + this.nodeWidthHalf} y="20" textAnchor="middle" style={{fontWeight:'bold'}}>Actor Servers</text>
        <rect key={++this.key} x={this.nodeServerX} y={this.nodeY} rx={radius} ry={radius} width={this.nodeWidth} height={this.nodeHeight} style={{fill:"#f9ffff",stroke:"black",strokeWidth:2}} />
      </>
    );
  }

  renderClient(virtualNetworks) {
    const clientAddressesSvg = virtualNetworks.map((virtualNetwork, virtualNetworkIndex) => {
      return virtualNetwork.clients.map((client, addressIndex) => {
        return this.renderClientAddress(client, virtualNetworkIndex, addressIndex);
      });
    });
    const clientNetworksSvg = virtualNetworks.map((virtualNetwork, index) => {
      return this.renderClientNetwork(virtualNetwork, index);
    });
    const clientPings = virtualNetworks.map((virtualNetwork, virtualNetworkIndex) => {
      return virtualNetwork.clients.map((client, addressIndex) => {
        return this.renderClientPing(client, this.clientAddressesX, this.networkYs[virtualNetworkIndex] + this.addressBias + (2 * this.addressBias * addressIndex));
      });
    });
    return (
      <g>
        {clientAddressesSvg}
        {clientNetworksSvg}
        {clientPings}
        {this.renderNodeClient()}
      </g>
    );
  }
  
  renderSut(virtualNetworks) {
    const sutClientAddressesSvg = virtualNetworks.map((virtualNetwork, virtualNetworkIndex) => {
      let addressIndex = 0;
      return virtualNetwork.suts.map((sutClient) => {
        if('client' === sutClient.direction || 'client/server' === sutClient.direction || 'server/client' === sutClient.direction) {
          return this.renderSutClientAddress(sutClient, virtualNetworkIndex, addressIndex++);
        }
      });
    });
    const sutServerAddressesSvg = virtualNetworks.map((virtualNetwork, virtualNetworkIndex) => {
      let addressIndex = 0;
      return virtualNetwork.suts.map((sutServer) => {
        if('server' === sutServer.direction || 'client/server' === sutServer.direction || 'server/client' === sutServer.direction) {
          return this.renderSutServerAddress(sutServer, virtualNetworkIndex, addressIndex++);
        }
      });
    });
    const sutServerPings = virtualNetworks.map((virtualNetwork, virtualNetworkIndex) => {
      let addressIndex = 0;
      return virtualNetwork.suts.map((sutServer) => {
        if('server' === sutServer.direction || 'client/server' === sutServer.direction || 'server/client' === sutServer.direction) {
          return this.renderSutServerPing(sutServer, this.sutServerAddressesX, this.networkYs[virtualNetworkIndex] + this.addressBias + (2 * this.addressBias * addressIndex++));
        }
      });
    });
    const sutClientPing = virtualNetworks.map((virtualNetwork, virtualNetworkIndex) => {
      let addressIndex = 0;
      return virtualNetwork.suts.map((sutClient) => {
        if('client' === sutClient.direction || 'client/server' === sutClient.direction || 'server/client' === sutClient.direction) {
          return this.renderSutClientPing(sutClient, this.sutClientAddressesX, this.networkYs[virtualNetworkIndex] + this.addressBias + (2 * this.addressBias * addressIndex++));
        }
      });
    });
    const sutServerNetworksSvg = virtualNetworks.map((virtualNetwork, index) => {
      return this.renderSutServerNetwork(virtualNetwork, index);
    });
    const sutClientNetworksSvg = virtualNetworks.map((virtualNetwork, index) => {
      return this.renderSutClientNetwork(virtualNetwork, index);
    });
    return (
      <g>
        {sutServerAddressesSvg}
        {sutServerNetworksSvg}
        {sutServerPings}
        {sutClientAddressesSvg}
        {sutClientNetworksSvg}
        {sutClientPing}
        {this.renderNodeSut()}
      </g>
    );
  }
  
  renderServer(virtualNetworks) {
    const serverAddressesSvg = virtualNetworks.map((virtualNetwork, virtualNetworkIndex) => {
      return virtualNetwork.servers.map((server, addressIndex) => {
        return this.renderServerAddress(server, virtualNetworkIndex, addressIndex);
      });
    });
    const serverNetworksSvg = virtualNetworks.map((virtualNetwork, index) => {
      return this.renderServerNetwork(virtualNetwork, index);
    });
    const serverPings = virtualNetworks.map((virtualNetwork, virtualNetworkIndex) => {
      return virtualNetwork.servers.map((server, addressIndex) => {
        return this.renderServerPing(server, this.serverAddressesX, this.networkYs[virtualNetworkIndex] + this.addressBias + (2 * this.addressBias * addressIndex));
      });
    });
    return (
      <g>
        {serverAddressesSvg}
        {serverNetworksSvg}
        {serverPings}
        {this.renderNodeServer()}
      </g>
    );
  }

  renderClouds(virtualNetworks) {
    const networks = virtualNetworks.map((virtualNetwork, index) => {
      return (
        <g key={index}>
          <text key={++this.key} x={this.nodeClientX + this.nodeWidth + 32} y={this.networkYs[index] - 20} style={{fontWeight:'bold'}}>
            {virtualNetwork.networkName}, {virtualNetwork.family}, {virtualNetwork.subnet}, {virtualNetwork.description}
          </text>
          <text key={++this.key} x={this.nodeSutX + this.nodeWidth + 32} y={this.networkYs[index] - 20} style={{fontWeight:'bold'}}>
            {virtualNetwork.networkName}, {virtualNetwork.family}, {virtualNetwork.subnet}, {virtualNetwork.description}
          </text>
          <rect key={++this.key} className={this._networkClass(virtualNetwork)} x={this.nodeClientX + this.nodeWidth + this.cloudXBias} y={this.networkYs[index] - this.cloudYBias} rx="4" ry="8" width={this.nodeSutX - (this.nodeClientX + this.nodeWidth) - 2 * this.cloudXBias} height={this.virtualNetworksHeight[index] + 2 * this.cloudYBias} />
          <rect key={++this.key} className={this._networkClass(virtualNetwork)} x={this.nodeSutX + this.nodeWidth + this.cloudXBias} y={this.networkYs[index] - this.cloudYBias} rx="4" ry="8" width={this.nodeServerX - (this.nodeSutX + this.nodeWidth) - 2 * this.cloudXBias} height={this.virtualNetworksHeight[index] + 2 * this.cloudYBias} />
        </g>
      );
    });
    return (
      <g>
        {networks}
      </g>
    );
  }
  
  renderInfoInterface(indexX, indexY, classNamePrefix, className, text, link) {
    const width = 230;
    const xBias = 120;
    const textY = 5;
    return (
      <g>
        <circle className="outer_address" cx={this.nodeClientX + xBias + width * indexX} cy={this.bottom + 30 * indexY} r={this.InterfaceRadiusOuter} />
        <circle className={`${classNamePrefix}_${className}`} cx={this.nodeClientX + xBias + width * indexX} cy={this.bottom + 30 * indexY} r={this.InterfaceRadiusInner} />
        <text key={++this.key} x={this.nodeClientX + xBias + width * indexX + 16} y={this.bottom + 30 * indexY + textY} textAnchor="left" style={{fontWeight:'bold'}}>
          {text}
        </text>
      </g>
    );
  }
  
  renderInfos() {
    let indexDynamic = -1;
    let indexStatic = -1;
    return (
      <>
      {this.renderInfoInterface(++indexDynamic, 0, 'dynamic', 'address_success', 'Dynamic Chosen Address')}
      {this.renderInfoInterface(++indexDynamic, 0, 'dynamic', 'address_reduced', 'Dynamic Redused Address')}
      {this.renderInfoInterface(++indexDynamic, 0, 'dynamic', 'address_multicast', 'Dynamic Multicast Address')}
      {this.renderInfoInterface(++indexDynamic, 0, 'dynamic', 'address_external', 'Dynamic External Address')}
      {this.renderInfoInterface(++indexDynamic, 0, 'dynamic', 'address_failure', 'Dynamic Failed Address')}
      {this.renderInfoInterface(++indexStatic, 1, 'static', 'address_success', 'Static Chosen Address')}
      {this.renderInfoInterface(++indexStatic, 1, 'static', 'address_reduced', 'Static Redused Address')}
      {this.renderInfoInterface(++indexStatic, 1, 'static', 'address_multicast', 'Static Multicast Address')}
      {this.renderInfoInterface(++indexStatic, 1, 'static', 'address_external', 'Static External Address')}
      {this.renderInfoInterface(++indexStatic, 1, 'static', 'address_failure', 'Static Failed Address')}
      </>
    );
  }
  
  render() {
    const virtualNetworks = this.props.virtualNetworks;
    this._calculate(virtualNetworks);
    const viewBox = [0, 0, this.nodeServerX + this.nodeWidth + this.nodeClientX, this.nodeHeight + 2 * this.nodeY + this.helpHeight].join(' ');
    return (
      <svg width={this.props.width} height={this.props.height} viewBox={viewBox}>
        {this.renderClouds(virtualNetworks)}
        {this.renderClient(virtualNetworks)}
        {this.renderSut(virtualNetworks)}
        {this.renderServer(virtualNetworks)}
        {this.renderInfos()}
      </svg>
    );
  }
  
  _calculate(virtualNetworks) {
    this.virtualNetworksHeight = [];
    this.networkYs = [];
    this.bottom = 0;
    this.nodeHeight = this.networkBias * (virtualNetworks.length + 1);
    if(virtualNetworks.length >= 2) {
      this.nodeHeight += this.networkExtraBias * (virtualNetworks.length - 1);
    }
    let top = this.nodeY + this.networkBias;
    for(let i = 0; i < virtualNetworks.length; ++i) {
      this.virtualNetworksHeight.push(2 * this.addressBias * Math.max(1, this._calculateSutLength(virtualNetworks[i].suts), virtualNetworks[i].servers.length, virtualNetworks[i].clients.length));
      this.nodeHeight += this.virtualNetworksHeight[i];
      this.networkYs.push(top);
      top += this.virtualNetworksHeight[i] + this.networkBias + this.networkExtraBias;
    }
    this.bottom = top;
  }
  
  _calculateSutLength(suts) {
    let server = 0;
    let client = 0;
    suts.forEach((sut) => {
      if('server' === sut.direction) {
        ++server;
      }
      else if('client' === sut.direction) {
        ++client;
      }
      else if('server/client' === sut.direction) {
        ++server;
        ++client;
      }
      else if('client/server' === sut.direction) {
        ++server;
        ++client;
      }
    });
    return Math.max(server, client);
  }
  
  
  _pingResultClass(address) {
    if(!this.props.pingResults || 0 === this.props.pingResults.size) {
      return 'ping_none';
    }
    const pingResult = this.props.pingResults.get(address);
    if(undefined === pingResult) {
      return 'ping_undefined';
    }
    else if(pingResult.result) {
      return 'ping_success';
    }
    else {
      return 'ping_failure';
    }
  }
  
  _addressPrefixClass(address) {
    if(address.static) {
      return 'static_';
    }
    else {
      return 'dynamic_';
    }
  }
  
  _addressPostfixClass(address) {
    if(address.valid) {
      if(address.reduced) {
        return 'address_reduced';
      }
      else if(address.multicast) {
        return 'address_multicast';
      }
      else {
        return 'address_success';
      }
    }
    else if(address.external && ((address.validAddress && address.validNetmask && address.validFamily)) || (address.multicast && address.validAddress && address.validFamily)) {
      return 'address_external';
    }
    return 'address_failure';
  }
  
  _addressClass(address) {
    return `${this._addressPrefixClass(address)}${this._addressPostfixClass(address)}`;
  }
  
  _networkClass(network) {
    if(network.valid) {
      if(network.reduced) {
         return 'network_reduced';
      }
      else {
        return 'network_success';
      }
    }
    else {
      return 'network_failure';
    }
  }
  
  _networkPopover(virtualNetwork) {
    const popup = [['name:', virtualNetwork.networkName], ['family:', virtualNetwork.family], ['subnet:', virtualNetwork.subnet]];
    if(virtualNetwork.valid) {
      if(virtualNetwork.reduced) {
        popup.push(['reduced', '']);
      }
    }
    else {
      popup.push(['not valid', '']);
    }
    return popup;
  }
}
