
'use strict';

import ComponentEmbed from './component-embed';
import ComponentLocalNote from './component-local-note';
import ComponentMarkup from './component-markup';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import Link from 'z-abs-complayer-router-client/project/client/react-component/link';
import React from 'react';


class ComponentDocument extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.refMarkup = React.createRef();
  }
  
  calculateTop(scrollData) {
    this.refMarkup.current.calculateTop(scrollData);
  }
  
  calculateObject(scrollData, whichObject) {
    this.refMarkup.current.calculateObject(scrollData, whichObject);
  }
  
  scroll(scrollData) {
    this.refMarkup.current.scroll(scrollData);
  }
    
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  renderEmbedMenu(value) {
    if(this.props.noediting) {
      return null;
    }
    else {
      return (
        <ComponentEmbed path={value.value.path} />
      );
    }
  }
  
  render() {
    return (
      <ComponentMarkup ref={this.refMarkup} inner={this.props.inner} preview={this.props.preview} document={this.props.document} linkReferences={this.props.linkReferences}
        onUnkownMarkup={(value, index, dataLineStart, dataLineStop, dataLineObjectIndex) => {
          if('markup_embed' === value.type) {
            if(!this.props.embededDocuments.has(value.value.path)) {
              return null;
            }
            else {
              return (
                <React.Fragment key={index}>
                 {this.renderEmbedMenu(value)}
                  <ComponentDocument noediting={this.props.noediting}  inner preview={this.props.preview} document={this.props.embededDocuments.get(value.value.path)} linkReferences={this.props.linkReferences} embededDocuments={this.props.embededDocuments} localNotes={this.props.localNotes} onAddNote={this.props.onAddNote} onEditNote={this.props.onEditNote} onDeleteNote={this.props.onDeleteNote} />
                </React.Fragment>  
              );
            }
          }
          else if('markup_local_note' === value.type) {
            if(this.props.noediting) {
              return null;
            }
            else {
              return (
                <ComponentLocalNote inner key={index} guid={value.value.guid} note={this.props.localNotes.get(value.value.guid)} dataLineStart={dataLineStart} dataLineStop={dataLineStop} dataLineObjectIndex={dataLineObjectIndex}
                  onAddNote={() => {
                    this.props.onAddNote && this.props.onAddNote(value.value.guid);
                  }}
                  onEditNote={() => {
                    this.props.onEditNote && this.props.onEditNote(value.value.guid);
                  }}
                  onDeleteNote={() => {
                    this.props.onDeleteNote && this.props.onDeleteNote(value.value.guid);
                  }}
                />
              );
            }
          }
          else {
            return (
              <p key={index}> UNKNOWN MARKUP KEY {value.type} </p>
            );
          }
        }}
        
        onEditorScroll={(top, line, delta, object) => {
          this.props.onEditorScroll && this.props.onEditorScroll(top, line, delta, object);
        }}
      />
    );
  }
}

module.exports = ComponentDocument;
