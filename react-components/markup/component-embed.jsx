
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import Link from 'z-abs-complayer-router-client/project/client/react-component/link';
import React from 'react';


class ComponentEmbed extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  renderEditEmbed() {
    return (
      <li className="embed_document_choice">
        <Link href={`/${this.props.path}`}>Edit Embed</Link>
      </li>
    );  
  }
  
  renderEmbedMenu() {
    return (
      <div className="dropdown embed_document_choice">
        <button className="btn btn-xs dropdown-toggle embed_document_choice" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          <span className="caret"></span>
        </button>
        <ul className="dropdown-menu dropdown-menu-right embed_document_choice" aria-labelledby="dropdownMenu1">
          {this.renderEditEmbed()}
        </ul>
      </div>
    );
  }
  
  render() {
    return (
      <>
        {this.renderEmbedMenu()}
      </>
    );
  }
}

module.exports = ComponentEmbed;
