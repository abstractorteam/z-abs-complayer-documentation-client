
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class ComponentImageSequenceDiagram extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  renderTitle(title, x) {
    if(0 !== title) {
      return (
        <text x={x} y={ComponentImageSequenceDiagram.TITLE_TOP} textAnchor="middle" style={{fontSize:ComponentImageSequenceDiagram.TITLE_FONT_SIZE,fontWeight:'bold'}}>
          {title}
        </text>
      );
    }
  }
  
  renderNodes(nodes, height, titleHeight) {
    let svgNodes = nodes.map((node, index) => {
      let x = ComponentImageSequenceDiagram.WIDTH_BIAS + index * ComponentImageSequenceDiagram.WIDTH_BETWEEN_NODES;
      return (
        <g key={index}>
          <text x={x} y={titleHeight + ComponentImageSequenceDiagram.HEIGHT_BIAS + ComponentImageSequenceDiagram.TEXT_BIAS_Y} style={{fontSize:'12px',fontWeight:'bold',textAnchor:'middle'}}>
            {node.name}
          </text>
          <line x1={x} y1={ComponentImageSequenceDiagram.HEIGHT_BIAS + titleHeight} x2={x} y2={ComponentImageSequenceDiagram.HEIGHT_BIAS + height + titleHeight} style={{stroke:'#666',strokeWidth:ComponentImageSequenceDiagram.NODE_WIDTH}} />
        </g>
      );
    });
    return (
      <g>
        {svgNodes}
      </g>
    );
  }
  
  renderMessageLine(message, x1, x2, y) {
    let xa;
    let xb;
    if('normal-bi-directional' === message.messageType || 'part-bi-directional' === message.messageType) {
      if(x1 < x2) {
        xa = x1 + ComponentImageSequenceDiagram.LINE_BIAS;
        xb = x2 - ComponentImageSequenceDiagram.LINE_BIAS;
      }
      else {
        xa = x1 - ComponentImageSequenceDiagram.LINE_BIAS;
        xb = x2 + ComponentImageSequenceDiagram.LINE_BIAS;
      }
    }
    else {
      if(x1 < x2) {
        xa = x1 + ComponentImageSequenceDiagram.NODE_WIDTH / 2;
        xb = x2 - ComponentImageSequenceDiagram.LINE_BIAS;
      }
      else {
        xa = x1 - ComponentImageSequenceDiagram.NODE_WIDTH / 2;
        xb = x2 + ComponentImageSequenceDiagram.LINE_BIAS;
      }
    }
    let type = `seq_dia_protocol_${message.type}`;
    if('part' === message.messageType || 'part-bi-directional' === message.messageType) {
     type += ' seq_message_part';
    }
    return (
      <line className={type} x1={xa} y1={y} x2={xb} y2={y} style={{strokeWidth:ComponentImageSequenceDiagram.NODE_WIDTH}} />
    );
  }
  
  renderTriangleEndPoints(x1, x2, y) {
    let xa1, xa2;
    let xb1, xb2;
    if(x1 < x2) {
      xa1 = x1 + ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb1 = x1 + ComponentImageSequenceDiagram.TRIANGLE_TOP;
      xa2 = x2 - ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb2 = x2 - ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    else {
      xa1 = x1 - ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb1 = x1 - ComponentImageSequenceDiagram.TRIANGLE_TOP;
      xa2 = x2 + ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb2 = x2 + ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    let trinagle1 = `${xa1},${y - ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xa1},${y + ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xb1},${y}`;
    let trinagle2 = `${xa2},${y - ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xa2},${y + ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xb2},${y}`;
    return (
      <g>
        <polygon points={trinagle1}/>
        <polygon points={trinagle2}/>
      </g>
    );
  }
  
  renderTriangleEndPoint(x1, x2, y) {
    let xa;
    let xb;
    if(x1 < x2) {
      xa = x2 - ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb = x2 - ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    else {
      xa = x2 + ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb = x2 + ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    let trinagle = `${xa},${y - ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xa},${y + ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xb},${y}`;
    return (
      <polygon points={trinagle}/>
    );
  }
  
  renderConnectEndPoints(x1, x2, y) {
    let xa;
    let xb;
    if(x1 < x2) {
      xa = x2 - ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb = x2 - ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    else {
      xa = x2 + ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb = x2 + ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    let trinagle = `${xa},${y - ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xa},${y + ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xb},${y}`;
    return (
      <g>
        <polygon points={trinagle} />
        <circle cx={x1} cy={y} r="5"/>
        <circle cx={x2} cy={y} r="5"/>
      </g>
    );    
  }
  
  renderDisconnectEndPoints(x1, x2, y) {
    let xa;
    let xb;
    if(x1 < x2) {
      xa = x2 - ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb = x2 - ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    else {
      xa = x2 + ComponentImageSequenceDiagram.TRIANGLE_BASE;
      xb = x2 + ComponentImageSequenceDiagram.TRIANGLE_TOP;
    }
    let trinagle = `${xa},${y - ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xa},${y + ComponentImageSequenceDiagram.TRIANGLE_HEIGHT} ${xb},${y}`;
    let x1Plus = x1 + ComponentImageSequenceDiagram.CROSS_LENGTH;
    let x1Minus = x1 - ComponentImageSequenceDiagram.CROSS_LENGTH;
    let x2Plus = x2 + ComponentImageSequenceDiagram.CROSS_LENGTH;
    let x2Minus = x2 - ComponentImageSequenceDiagram.CROSS_LENGTH;
    let yPlus = y + ComponentImageSequenceDiagram.CROSS_LENGTH;
    let yMinus = y - ComponentImageSequenceDiagram.CROSS_LENGTH;
    return (
      <g>
        <polygon points={trinagle} />
        <line x1={x1Minus} y1={yPlus} x2={x1Plus} y2= {yMinus} style={{stroke:'black',strokeWidth:ComponentImageSequenceDiagram.NODE_WIDTH}} />
        <line x1={x1Plus} y1={yPlus} x2={x1Minus} y2= {yMinus} style={{stroke:'black',strokeWidth:ComponentImageSequenceDiagram.NODE_WIDTH}} />
        <line x1={x2Minus} y1={yPlus} x2={x2Plus} y2= {yMinus} style={{stroke:'black',strokeWidth:ComponentImageSequenceDiagram.NODE_WIDTH}} />
        <line x1={x2Plus} y1={yPlus} x2={x2Minus} y2= {yMinus} style={{stroke:'black',strokeWidth:ComponentImageSequenceDiagram.NODE_WIDTH}} />
      </g>
    );    
  }
  
  renderMessageEndPoints(message, x1, x2, y) {
    if('normal' === message.messageType || 'part' === message.messageType) {
      return this.renderTriangleEndPoint(x1, x2, y);
    }
    else if('normal-bi-directional' === message.messageType || 'part-bi-directional' === message.messageType) {
      return this.renderTriangleEndPoints(x1, x2, y);
    }
    else if('connect' === message.messageType) {
      return this.renderConnectEndPoints(x1, x2, y);
    }
    else if('disconnect' === message.messageType) {
      return this.renderDisconnectEndPoints(x1, x2, y);
    }
  }
  
  renderMessageData(message, x1, x2, y) {
    let x = 0;
    let anchor = '';
    if(x1 < x2) {
      x = x1 + ComponentImageSequenceDiagram.TEXT_BIAS_X;
      anchor = 'start';
    }
    else {
      x = x1 - ComponentImageSequenceDiagram.TEXT_BIAS_X;
      anchor = 'end';
    }
    return ( 
      <text x={x} y={y + ComponentImageSequenceDiagram.TEXT_BIAS_Y} style={{fontSize:'10px',fontWeight:'bold',textAnchor:anchor}}>
        {message.data}
      </text>
    );
  }
  
  renderMessages(messages, titleHeight) {
    let svgMessages = messages.map((message, index) => {
      let x1 = ComponentImageSequenceDiagram.WIDTH_BIAS + message.fromIndex * ComponentImageSequenceDiagram.WIDTH_BETWEEN_NODES;
      let x2 = ComponentImageSequenceDiagram.WIDTH_BIAS + message.toIndex * ComponentImageSequenceDiagram.WIDTH_BETWEEN_NODES;
      let y = titleHeight + ComponentImageSequenceDiagram.HEIGHT_BIAS + ComponentImageSequenceDiagram.NODE_HEIGHT * (index + 1);
      return (
        <g key={index}>
          {this.renderMessageData(message, x1, x2, y)}
          {this.renderMessageLine(message, x1, x2, y)}
          {this.renderMessageEndPoints(message, x1, x2, y)}
        </g>
      );
    });
    return (
      <g>
        {svgMessages}
      </g>
    );
  }
  
  render() {
    let title = this.props.sequenceDiagram.title;
    let nodes = this.props.sequenceDiagram.nodes;
    let messages = this.props.sequenceDiagram.messages;
    let titleHeight = ('' !== title && undefined !== title) ? ComponentImageSequenceDiagram.TITLE_HEIGHT : 0;
    let nodeHeight = ComponentImageSequenceDiagram.HEIGHT_BIAS + ComponentImageSequenceDiagram.NODE_HEIGHT + (messages.length - 1) * ComponentImageSequenceDiagram.NODE_HEIGHT;
    let svgHeight = titleHeight + nodeHeight + ComponentImageSequenceDiagram.NODE_HEIGHT + ComponentImageSequenceDiagram.HEIGHT_BIAS;
    let svgWidth = 2 * ComponentImageSequenceDiagram.WIDTH_BIAS + ComponentImageSequenceDiagram.WIDTH_BETWEEN_NODES * (nodes.length - 1);
    const viewBox = [0, 0, svgWidth, svgHeight].join(' ');
    return (
      <svg className="markup_seq" width={this.props.width} height={svgHeight} viewBox={viewBox}>
        {this.renderTitle(title, svgWidth / 2)}
        {this.renderNodes(nodes, nodeHeight, titleHeight)}
        {this.renderMessages(messages, titleHeight)}
      </svg>
    );
  }
  
/*  _networkPopover(virtualNetwork) {
    let popup = [['name:', virtualNetwork.name], ['family:', virtualNetwork.family], ['subnet:', virtualNetwork.subnet]];
    if(virtualNetwork.valid) {
      if(virtualNetwork.reduced) {
        popup.push(['reduced', '']);
      }
    }
    else {
      popup.push(['not valid', '']);
    }
    return popup;
  }*/
}

ComponentImageSequenceDiagram.TITLE_HEIGHT = 60;
ComponentImageSequenceDiagram.TITLE_FONT_SIZE = 20;
ComponentImageSequenceDiagram.TITLE_TOP = ComponentImageSequenceDiagram.TITLE_FONT_SIZE + (ComponentImageSequenceDiagram.TITLE_HEIGHT - ComponentImageSequenceDiagram.TITLE_FONT_SIZE) / 2;
ComponentImageSequenceDiagram.WIDTH_BIAS = 30;
ComponentImageSequenceDiagram.HEIGHT_BIAS = 30;
ComponentImageSequenceDiagram.NODE_WIDTH = 3;
ComponentImageSequenceDiagram.NODE_HEIGHT = 17;
ComponentImageSequenceDiagram.WIDTH_BETWEEN_NODES = 150;
ComponentImageSequenceDiagram.TEXT_BIAS_X = 12;
ComponentImageSequenceDiagram.TEXT_BIAS_Y = -4;
ComponentImageSequenceDiagram.TRIANGLE_BASE = 12;
ComponentImageSequenceDiagram.TRIANGLE_TOP = (ComponentImageSequenceDiagram.NODE_WIDTH - 1) / 2;
ComponentImageSequenceDiagram.TRIANGLE_HEIGHT = 4;
ComponentImageSequenceDiagram.LINE_BIAS = ComponentImageSequenceDiagram.TRIANGLE_BASE - ComponentImageSequenceDiagram.TRIANGLE_TOP;
ComponentImageSequenceDiagram.CROSS_LENGTH = 5;
