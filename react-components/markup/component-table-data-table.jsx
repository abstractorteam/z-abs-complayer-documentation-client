
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import Link from 'z-abs-complayer-router-client/project/client/react-component/link';
import React from 'react';


export default class ComponentTableDataTable extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.showAlways, nextProps.showAlways)
      || !this.shallowCompare(this.props.classTable, nextProps.classTable)
      || !this.shallowCompare(this.props.classHeading, nextProps.classHeading)
      || !this.shallowCompare(this.props.classRow, nextProps.classRow)
      || !this.shallowCompare(this.props.dataTable, nextProps.dataTable)
      || !this.shallowCompare(this.props.filter, nextProps.filter)
      || !this.deepCompare(this.props.values, nextProps.values);
  }
  
  renderLink(analyzedValue) {
    if(!this.props.preview) {
      return (
        <Link global href={analyzedValue.link}>
          {analyzedValue.value}
        </Link>
      );
    }
    else {
      return (
        <a className="doc_nav_preview">
          {analyzedValue.value}
        </a>
      );
    }
  }
  
  renderTableRowValue(markupTableHeading, value, index) {
    const analyzedValue = this.props.dataTable.getValue(markupTableHeading, value);
    if(!analyzedValue.link) {
      return (
        <td key={index} style={analyzedValue.style}>
          {analyzedValue.value}
        </td>
      );
    }
    else {
      return (
        <td key={index} style={analyzedValue.style}>
          {this.renderLink(analyzedValue)}
        </td>
      );
    }
  }
  
  renderTableRow(markupTableHeadings, value, index) {
    if(this.props.filter && !this.props.filter(value)) {
      return;
    }
    const values = markupTableHeadings.map((heading, index) => {
      return this.renderTableRowValue(heading, value, index);
    });
    return (
      <tr key={index}>
        <td>{index + 1}</td>
        {values}
      </tr>  
    );
  }
    
  renderTableRows(markupTableHeadings) {
    if(undefined !== this.props.values) {
      const values = this.props.values.map((value, index) => {
        return this.renderTableRow(markupTableHeadings, jQuery.extend(true, {}, value), index);
      });
      return (
        <tbody>
          {values}
        </tbody>
      );
    }
  }

  renderTableHeadings(markupTableHeadings) {
    const headings = markupTableHeadings.map((heading, index) => {
      return (<th key={index}>{heading}</th>);
    });
    return (
      <tr>
        <th>#</th>
        {headings}
      </tr>  
    );
  }

  render() {
    if(undefined !== this.props.values && (0 < this.props.values.length || this.props.hasOwnProperty('showAlways'))) {
      const markupTableName = this.props.dataTable.getName();
      const markupTableHeadings = this.props.dataTable.getHeadings();
      return (
        <div className={this.props.classTable}>
          <div className={`panel-heading ${this.props.classHeading}`}>{markupTableName}</div>
          <table className={`${this.props.classRow} table table-bordered table-striped table-condensed table-hover`}>
            <thead>
              {this.renderTableHeadings(markupTableHeadings)}
            </thead>
            {this.renderTableRows(markupTableHeadings)}
          </table>
        </div>
      );
    }
    else {
      return null;
    }
  }
}
