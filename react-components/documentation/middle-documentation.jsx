
'use strict';

import MiddleDocumentationToolbar from './middle-documentation-toolbar';
import MiddleDocumentationView from './middle-documentation-view';
import Route from 'z-abs-complayer-router-client/project/client/react-component/route';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class MiddleDocumentation extends ReactComponentBase {
  constructor(props) {
    super(props, {
      showNavigationMarkup: false,
      showNavigationPreview: false,
      showDocumentationMarkup: true,
      showDocumentationPreview: true,
      documentationOrder: true
    });
    this.setTitle(() => {
      return {
        isPath: this.props._uriPath,
        text: this.props._uriPath ? this.props._uriPath : 'Documentation',
        prefix: 'DOC: '
      };
    });
  }
    
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps)
      || !this.shallowCompare(this.state, nextState);
  }
  
  renderToolbar() {
    if(!this.props.noediting) {
      return (
        <MiddleDocumentationToolbar documentationStore={this.props.documentationStore} titleText={this.props.titleText} showNavigationMarkup={this.state.showNavigationMarkup} showDocumentationMarkup={this.state.showDocumentationMarkup} showDocumentationPreview={this.state.showDocumentationPreview} showNavigationPreview={this.state.showNavigationPreview} documentationOrder={this.state.documentationOrder}
          onToggleNavigationMarkup={() => {
            this.updateState({
              showNavigationMarkup: {$set: !this.state.showNavigationMarkup}
            });
          }}
          onToggleNavigationPreview={() => {
            this.updateState({
              showNavigationPreview: {$set: !this.state.showNavigationPreview}
            });
          }}
          onToggleDocumentationMarkup={() => {
            this.updateState({
              showDocumentationMarkup: {$set: !this.state.showDocumentationMarkup}
            });
          }}
          onToggleDocumentationPreview={() => {
            this.updateState({
              showDocumentationPreview: {$set: !this.state.showDocumentationPreview}
            });
          }}
          onToggleDocumentationOrder={() => {
            this.updateState({
              documentationOrder: {$set: !this.state.documentationOrder}
            });
          }}
        />
      );
    }
  }
  
  renderSidebar() {
    const Sidebar = this.props.sidebar;
    if(Sidebar) {
      return (
        <Sidebar />
      );
    }
  }
  
  render() {
    return (
      <div className="documentation_view_middle">
        {this.renderToolbar()}
        {this.renderSidebar()}
        <Route switch="*" catchClick location={this.props.location} handler={MiddleDocumentationView} className={`documentation_view_middle_view${undefined !== this.props.sidebar ? ' documentation_view_middle_view_with_sidebar' : ''}${this.props.noediting ? '' : ' documentation_view_middle_view_with_toolbar'}`}  noediting={this.props.noediting} documentationStore={this.props.documentationStore} titleText={this.props.titleText} showNavigationMarkup={this.state.showNavigationMarkup} showDocumentationMarkup={this.state.showDocumentationMarkup} showDocumentationPreview={this.state.showDocumentationPreview} showNavigationPreview={this.state.showNavigationPreview} documentationOrder={this.state.documentationOrder} />
      </div>
    );
  }
}
