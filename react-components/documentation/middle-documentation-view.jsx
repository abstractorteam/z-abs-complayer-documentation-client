
'use strict';

import MiddleDocumentationNavigation from './middle-documentation-navigation';
import { ActionDocumentationCurrentPage, ActionDocumentationGotoPage, ActionDocumentationMarkupNavigationChange, ActionDocumentationMarkupDocumentChange, ActionDocumentationMarkupCancel, ActionDocumentationLocalNoteMarkup, ActionDocumentationLocalNoteMarkupCancel, ActionDocumentationLocalNoteMarkupChange, ActionDocumentationLocalNoteDelete } from '../../actions/action-documentation';
import ComponentMarkedTextarea from 'z-abs-complayer-bootstrap-client/marked-textarea';
import DataDocumentationNavigation from 'z-abs-complayer-documentation-client/data/data-documentation/data-documentation-navigation';
import ComponentDocument from 'z-abs-complayer-documentation-client/react-components/markup/component-document';
import ScrollData from 'z-abs-complayer-documentation-client/react-components/helper/scroll-data';
import ComponentBootstrapButton from 'z-abs-complayer-bootstrap-client/button';
import NotFound from 'z-abs-corelayer-client/components/not-found';
import {RouterContext} from 'z-abs-complayer-router-client/project/client/react-component/router-context';
import ReactComponentStore from 'z-abs-corelayer-client/react-component/react-component-store';
import React from 'react';


export default class MiddleDocumentationView extends ReactComponentStore {
  constructor(props) {
    super(props, [props.documentationStore], {
      autoScroll: true,
      breaks: false,
      whiteSpaceClass: 'no_word_wrap'
    });
    this.boundKeyDown = this._keyDown.bind(this);
    this.boundKeyUp = this._keyUp.bind(this);
    this.ctrlKey = false;
    this.refDocumentDiv = React.createRef();
    this.refDocumentComponentDocument = React.createRef();
    this.refPreviewDiv = React.createRef();
    this.refPreviewComponentDocument = React.createRef();
    this.refMarkupComponentMarkedTextarea = React.createRef();
    this.scrollData = new ScrollData();
    this.documentationStoreName = props.documentationStore.constructor.name;
    this.scrollGuard = false;
    this.topGuard = false;
  }
  
  _documentationStore(state) {
    return Reflect.get(state, this.documentationStoreName);
  }
  
  didMount() {
    window.addEventListener('keydown', this.boundKeyDown, true);
    window.addEventListener('keyup', this.boundKeyUp, true);
    this.dispatch(this.props.documentationStore, new ActionDocumentationCurrentPage(this._getDocumentName(this.props._uriPath)));
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps)
      || !this.shallowCompare(this._documentationStore(this.state), this._documentationStore(nextState))
      || !this.shallowCompare(this.state.autoScroll, nextState.autoScroll)
      || !this.shallowCompare(this.state.breaks, nextState.breaks)
      || !this.shallowCompare(this.state.whiteSpaceClass, nextState.whiteSpaceClass);
  }
  
  willUpdate(nextProps, nextState) {
    if(!this._documentationStore(this.state).markup.definition) {
      if(null !== this.refDocumentComponentDocument.current) {
        this.refDocumentComponentDocument.current.calculateTop(this.scrollData);
      }
    }
    else {
      if(null !== this.refPreviewComponentDocument.current) {
        this.refPreviewComponentDocument.current.calculateTop(this.scrollData);
      }
    }
  }
  
  didUpdate(prevProps, prevState) {
    ++MiddleDocumentationView.ID;
    if(this._documentationStore(this.state).markup.definition) {
      if(!this._documentationStore(prevState).markup.definition || this.props.documentationOrder !== prevProps.documentationOrder) {
        const lines = this._documentationStore(this.state).markup.document.contentLines;
        const lineHeight = this.refMarkupComponentMarkedTextarea.current.refTextArea.current.scrollHeight / lines;
        const visibleLines = this.refMarkupComponentMarkedTextarea.current.refTextArea.current.clientHeight / lineHeight;
        this.scrollData.setLength(Math.floor(lines - visibleLines));
        this.refPreviewComponentDocument.current.scroll(this.scrollData);
        this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
      }
      else {
        const lineDelta = this._documentationStore(this.state).markup.document.contentLines - this._documentationStore(prevState).markup.document.contentLines;
        if(0 !== lineDelta) {
          this.scrollData.stepLines(lineDelta);
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }
      }
    }
    else {
      if(this._documentationStore(prevState).markup.definition) {
        if(null !== this.refDocumentComponentDocument.current) {
          this.refDocumentComponentDocument.current.scroll(this.scrollData);
        }
      }
      else {
        if(this.props.location.hash) {
          const hash = document.getElementById(this.props.location.hash.substring(1));
          if(null !== hash) {
            this.scrollGuard = true;
            hash.scrollIntoView();
          }
        }
        else {
          if(!this.topGuard) {
            const hashTop = document.getElementById('topOfPage');
            if(null !== hashTop) {
              this.scrollGuard = true;
              hashTop.scrollIntoView();
            }
          }
          else {
            this.topGuard = false;
          }
        }
      }
    }
    if(!this.shallowCompare(this.props._uriPath, prevProps._uriPath)) {
      const documentName = this._getDocumentName(this.props._uriPath)
      if(this._documentationStore(this.state).currentDocumentName !== documentName) {
        this.dispatch(this.props.documentationStore, new ActionDocumentationCurrentPage(documentName));
      }
    }
  }
  
  willUnmount() {
    window.removeEventListener('keyup', this.boundKeyUp, true);
    window.removeEventListener('keydown', this.boundKeyDown, true);
    if(this._documentationStore(this.state).markup.definition) {
      this.dispatch(this.props.documentationStore, new ActionDocumentationMarkupCancel());
    }
    if(this._documentationStore(this.state).localNoteMarkup.definition) {
      this.dispatch(this.props.documentationStore, new ActionDocumentationLocalNoteMarkupCancel());
    }
  }
  
  _keyDown(e) {
    if(e.ctrlKey) {
      this.ctrlKey = true;
    }
  }
  
  _keyUp(e) {
    if(!e.ctrlKey) {
      this.ctrlKey = false;
    }
  }
  
  _calculateColumnClass() {
    let columns = 0;
    this.props.showNavigationMarkup && ++columns;
    this.props.showNavigationPreview && ++columns;
    this.props.showDocumentationMarkup && ++columns;
    this.props.showDocumentationPreview && ++columns;
    if(!this.props.showNavigationPreview) {
      return `markup_filter_${this.props.documentationOrder ? 'horizontal' : 'vertical'}_${columns}`;
    }
    else {
      return `markup_filter_${this.props.documentationOrder ? 'horizontal_with' : 'vertical'}_${columns}`;
    }
  }
  
  renderNavigationMarkup() {
    return (
      <div className={this.props.showNavigationMarkup ? this._calculateColumnClass() : 'markup_filter_0'}>
        <form className="doc_same_size_as_parent" role="form">
          <div className="form-group test_case_form_group">
            <label htmlFor="comment">Navigation - Markup</label>
            <ComponentMarkedTextarea className={this.state.whiteSpaceClass + " form-control test_case_definition doc_same_size_as_parent"} rows="10" value={this._documentationStore(this.state).markup.navigation.content} results={this._documentationStore(this.state).markup.navigation.rows}
              onChange={(value) => {
                this.dispatch(this.props.documentationStore, new ActionDocumentationMarkupNavigationChange(value));
              }}
            />
          </div>
        </form>
      </div>
    );
  }
  
  renderNavigationPreview() {
    return (
      <div className={this.props.showNavigationPreview ? `${this.props.documentationOrder ? 'markup_filter_horizontal_with_1' : this._calculateColumnClass()}` : 'markup_filter_0'}>
        <label htmlFor="comment">Navigation - Preview</label>
        <div className="doc_nav_navigation_preview">
          <MiddleDocumentationNavigation documentationStore={this.props.documentationStore} preview />
        </div>
      </div>
    );
  }
  
  renderDocumentMarkupScrollFirstLine() {
    const disabled = !this.state.autoScroll;
    return (
      <ComponentBootstrapButton className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="First line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.setLineFirst();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderDocumentMarkupScrollPreviousBlock() {
    const disabled = !this.state.autoScroll;
    return (
      <ComponentBootstrapButton className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Previous block" disabled={disabled} sticky
        onClick={(e) => {
          this.refPreviewComponentDocument.current.calculateObject(this.scrollData, 'previous_block');
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-step-backward" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderDocumentMarkupScrollPreviousLine() {
    const disabled = !this.state.autoScroll;
    return (
      <ComponentBootstrapButton className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Previous line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.decrement();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderDocumentMarkupScrollNextLine() {
    const disabled = !this.state.autoScroll;
    return (
      <ComponentBootstrapButton className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Next line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.increment();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderDocumentMarkupScrollNextBlock() {
    const disabled = !this.state.autoScroll;
    return (
      <ComponentBootstrapButton className="markup_button_other" type="transparent" placement="bottom" heading="Goto" content="Next block" disabled={disabled} sticky
        onClick={(e) => {
          this.refPreviewComponentDocument.current.calculateObject(this.scrollData, 'next_block');
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-step-forward" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderDocumentMarkupScrollEndLine() {
    const disabled = !this.state.autoScroll;
    return (
      <ComponentBootstrapButton className="markup_button_right" type="transparent" placement="bottom" heading="Goto" content="End line" disabled={disabled} sticky
        onClick={(e) => {
          this.scrollData.setLineLast();
          this.refPreviewComponentDocument.current.scroll(this.scrollData);
          this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
        }}
      >
        <span className="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderDocumentMarkup() {
    return (
      <div className={this.props.showDocumentationMarkup ? this._calculateColumnClass() : 'markup_filter_0'}>
        <form className="doc_same_size_as_parent" role="form">
          <div className="form-group test_case_form_group">
            <div className="markup_checkbox">
              <label htmlFor="comment">Document - Markup</label>
              <p className="markup_checkbox">
                auto-scroll:
              </p>
              <input className="markup_checkbox" type="checkbox" aria-label="..." autoComplete="off" checked={this.state.autoScroll}
                onChange={(e) => {
                  if(this.state.autoScroll) {
                    this.updateState({
                      autoScroll: {$set: false}
                    });
                  }
                  else {
                    this.updateState({
                      autoScroll: {$set: true}
                    });
                  }
                }}
              />
              <p className="markup_checkbox">
                breaks:
              </p>
              <input className="markup_checkbox" type="checkbox" aria-label="..." autoComplete="off" checked={this.state.breaks}
                onChange={(e) => {
                  if(this.state.breaks) {
                    this.updateState({
                      breaks: {$set: false},
                      whiteSpaceClass: {$set: 'no_word_wrap'}
                    });
                  }
                  else {
                    this.updateState({
                      breaks: {$set: true},
                      whiteSpaceClass: {$set: 'word_wrap'}
                    });
                  }
                }}
              />
              {this.renderDocumentMarkupScrollEndLine()}
              {this.renderDocumentMarkupScrollNextBlock()}
              {this.renderDocumentMarkupScrollNextLine()}
              {this.renderDocumentMarkupScrollPreviousLine()}
              {this.renderDocumentMarkupScrollPreviousBlock()}
              {this.renderDocumentMarkupScrollFirstLine()}
            </div>
            <ComponentMarkedTextarea ref={this.refMarkupComponentMarkedTextarea} className={this.state.whiteSpaceClass + " form-control test_case_definition doc_same_size_as_parent"} value={this._documentationStore(this.state).markup.document.content}
              onScroll={(e) => {
                const lines = this._documentationStore(this.state).markup.document.contentLines;
                const scrollLines = (e.target.scrollHeight - e.target.clientHeight) / e.target.scrollHeight * lines;
                const scrollLine = e.target.scrollTop / (e.target.scrollHeight - e.target.clientHeight) * scrollLines;
                const line = Math.floor(scrollLine);
                this.scrollData.setLine(line, scrollLine - line);
                if(this.state.autoScroll) {
                  this.refPreviewComponentDocument.current.scroll(this.scrollData);
                }
              }}
              onChange={(value, rows) => {
                this.dispatch(this.props.documentationStore, new ActionDocumentationMarkupDocumentChange(value));
              }}
            />
          </div>
        </form>
      </div>
    );
  }
  
  renderDocumentationPreview() {
    const storeState = this._documentationStore(this.state).documentationPreview;
    return (
      <div className={this.props.showDocumentationPreview ? this._calculateColumnClass() : 'markup_filter_0'}>
        <label htmlFor="comment">Document - Preview</label>
        <div ref={this.refPreviewDiv} className="doc_nav_preview_outer"
          onScroll={(e) => {
            this.scrollData.setScroll(e.target.scrollTop);
          }}
        >
          <div className="doc_nav_preview_middle">
            <div className="doc_nav_preview_inner"
              onMouseUp={(e) => {
                if(this.ctrlKey) {
                  this.scrollData.setScroll(e.target.offsetTop);
                  this.refPreviewComponentDocument.current.calculateTop(this.scrollData);
                  this.refPreviewComponentDocument.current.scroll( this.scrollData);
                  this.refMarkupComponentMarkedTextarea.current.scroll(this.scrollData);
                }
              }}
            >
              <ComponentDocument noediting={this.props.noediting}  ref={this.refPreviewComponentDocument} preview document={storeState.document} embededDocuments={storeState.embededDocuments} localNotes={storeState.localNotes} linkReferences={this._documentationStore(this.state).linkReferences}
                onEditorScroll={(top) => {
                  this.refPreviewDiv.current.scrollTop = top;
                }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
  
  renderLocalNoteMarkup() {
    return (
      <div className={`markup_filter_${this.props.documentationOrder ? 'horizontal' : 'vertical'}_1`}>
        <form className="doc_same_size_as_parent" role="form">
          <div className="form-group test_case_form_group">
            <div className="markup_checkbox">
              <label htmlFor="comment">Markup - Local Note</label>
              <p className="markup_checkbox">
                breaks:
              </p>
              <input className="markup_checkbox" type="checkbox" aria-label="..." autoComplete="off" checked={this.state.breaks}
                onChange={(e) => {
                  if(this.state.breaks) {
                    this.updateState({
                      breaks: {$set: false},
                      whiteSpaceClass: {$set: 'no_word_wrap'}
                    });
                  }
                  else {
                    this.updateState({
                      breaks: {$set: true},
                      whiteSpaceClass: {$set: 'word_wrap'}
                    });
                  }
                }}
              />
            </div>
            <ComponentMarkedTextarea className={this.state.whiteSpaceClass + " form-control test_case_definition doc_same_size_as_parent"} rows="10" value={this._documentationStore(this.state).localNoteMarkup.document.content}
              onChange={(value) => {
                this.dispatch(this.props.documentationStore, new ActionDocumentationLocalNoteMarkupChange(value));
              }}
            />
          </div>
        </form>
      </div>
    );
  }
  
  renderDocument() {
    if(undefined === this._documentationStore(this.state).currentDocumentName) {
      return null;
    }
    const storeState = this._documentationStore(this.state).documentation;
    if(!this.props.noediting || (this.props.noediting && storeState.documentCreated)) {
      return (
        <>
          <div id="topOfPage" />
          <ComponentDocument noediting={this.props.noediting} ref={this.refDocumentComponentDocument} document={storeState.document} embededDocuments={storeState.embededDocuments} localNotes={storeState.localNotes} linkReferences={this._documentationStore(this.state).linkReferences}
            onAddNote={(guid) => {
              this.dispatch(this.props.documentationStore, new ActionDocumentationLocalNoteMarkup(guid));
            }}
            onEditNote={(guid) => {
              this.dispatch(this.props.documentationStore, new ActionDocumentationLocalNoteMarkup(guid));
            }}
            onDeleteNote={(guid) => {
              this.dispatch(this.props.documentationStore, new ActionDocumentationLocalNoteDelete(guid));
            }}
            onEditorScroll={(top) => {
              this.refDocumentDiv.current.scrollTop = top;
            }}
          />
          <div id="bottomOfPage" />
        </>
      );
    }
    else {
      return (
        <NotFound />
      );
    }
  }
  
  renderPreviousDocument() {
    const disabled = false;
    return (
      <ComponentBootstrapButton className="documentation_previous_document" type="transparent" placement="bottom" heading="Goto" content="Previous Document" disabled={disabled} sticky
        onClick={(e) => {
          this.dispatch(this.props.documentationStore, new ActionDocumentationGotoPage('previous'));
        }}
      >
        <span className="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderFirstDocument() {
    const disabled = false;
    return (
      <ComponentBootstrapButton className="documentation_first_document" type="transparent" placement="bottom" heading="Goto" content="First Document" disabled={disabled} sticky
        onClick={(e) => {
          this.dispatch(this.props.documentationStore, new ActionDocumentationGotoPage('first'));
        }}
      >
        <span className="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderNextDocument() {
    const disabled = false;
    return (
      <ComponentBootstrapButton className="documentation_next_document" type="transparent" placement="bottom" heading="Goto" content="Next Document" disabled={disabled} sticky
        onClick={(e) => {
          this.dispatch(this.props.documentationStore, new ActionDocumentationGotoPage('next'));
        }}
      >
        <span className="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  renderLastDocument() {
    const disabled = false;
    return (
      <ComponentBootstrapButton className="documentation_last_document" type="transparent" placement="bottom" heading="Goto" content="Last Document" disabled={disabled} sticky
        onClick={(e) => {
          this.dispatch(this.props.documentationStore, new ActionDocumentationGotoPage('last'));
        }}
      >
        <span className="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
      </ComponentBootstrapButton>
    );
  }
  
  render() {
    const className = `${this.props.className ? ' ' + this.props.className : ''}`;
    const documentationStore = this._documentationStore(this.state);
    if(documentationStore.markup.definition) {
      return (
        <div className={className}>
          <div className="documentation_tab_view">
            {this.renderNavigationMarkup()}
            {this.renderNavigationPreview()}
            {this.renderDocumentMarkup()}
            {this.renderDocumentationPreview()}
          </div>
        </div>
      );
    }
    else if(documentationStore.localNoteMarkup.definition) {
      return (
        <div className={className}>
          <div className="documentation_tab_view">
            {this.renderLocalNoteMarkup()}
          </div>
        </div>
      );
    }
    else {
      return (
        <div className={className}>
          <div className="middle_documentation_start_view doc_same_size_as_parent">
            <div className="container-fluid">
              <div className="row">
                <div ref={this.refDocumentDiv} className="col-xs-10 middle_view_documentaion_doc"
                  onScroll={(e) => {
                    this.scrollData.setScroll(e.target.scrollTop,e.target.scrollTop);
                    if(this.scrollGuard) {
                      this.scrollGuard = false;
                    }
                  }}
                >
                  <div className="col-xs-1" style={{position:'sticky',top:'0px'}}>
                    {this.renderFirstDocument()}
                    {this.renderPreviousDocument()}
                  </div>
                  <div className="col-xs-10">
                    {this.renderDocument()}
                  </div>
                  <div className="col-xs-1" style={{position:'sticky',top:'0px'}}>
                    {this.renderLastDocument()}
                    {this.renderNextDocument()}
                  </div>
                </div>
                <MiddleDocumentationNavigation documentationStore={this.props.documentationStore} titleText={this.props.titleText} _uriPath={this.props._uriPath} />
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
  
  _getDocumentName(path) {
    if(path.startsWith('/')) {
      path = path.substring(1);
    }
    return path.length !== 0 ? path : null;
  }
}

MiddleDocumentationView.contextType = RouterContext;
MiddleDocumentationView.ID = 0;

