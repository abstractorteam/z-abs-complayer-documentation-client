
'use strict';


class LineData {
  constructor(line, lineObject = null) {
    this.line = line;
    this.objects = lineObject ? [lineObject] : [];
  }
  
  addObject(lineObject) {
    this.objects.push(lineObject);
  }
}

module.exports = LineData;
