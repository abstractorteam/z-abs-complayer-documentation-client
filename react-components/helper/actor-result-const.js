
'use strict';

// DOUBLE CODE : TODO: remove
class ActorResultConst {
  static getTableNameResultIds(tableNameResult, emptyIsSuccess = false) {
    if(emptyIsSuccess && '' === tableNameResult) {
      return ActorResultConst.SUCCESS;
    }
    else {
      return _ActorResultConst_tableNameResultIds.get(tableNameResult);
    }
  }
  
  static getClassFromResult(result) {
    return _ActorResultConst_tableNameClasses.get(result);
  }
  
  static getName(resultId) {
    return ActorResultConst.results[resultId];
  }
  
  static getClass(resultId) {
    return ActorResultConst.classes[resultId];
  }
  
  static getClassLight(resultId) {
    return ActorResultConst.classesLight[resultId];
  }
}

ActorResultConst.results = [
  'None',
  'n/a',
  'n_impl',
  'Success',
  'n_exec',
  'Interrupt',
  'Failure',
  'Error',
  'Timeout',
  'Reflection'
];

ActorResultConst.tableNameResults = [
  'none',
  'n/a',
  'n_impl',
  'success',
  'n_exec',
  'interrupt',
  'failure',
  'error',
  'timeout',
  'reflection'
];

ActorResultConst.resultIds = [
  0,
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9
];

ActorResultConst.classes = [
  'test_none',
  'test_na',
  'test_nimpl',
  'test_success',
  'test_nexec',
  'test_interrupt',
  'test_failure',
  'test_error',
  'test_timeout',
  'test_reflection'
];

ActorResultConst.classesLight = [
  'test_none-light',
  'test_na-light',
  'test_nimpl-light',
  'test_success-light',
  'test_nexec-light',
  'test_interrupt-light',
  'test_failure-light',
  'test_error-light',
  'test_timeout-light',
  'test_reflection-light'
];

ActorResultConst.resultClasses = [
  'test_none_svg',
  'test_na_svg',
  'test_nimpl_svg',
  'test_success_svg',
  'test_nexec_svg',
  'test_interrupt_svg',
  'test_failure_svg',
  'test_error_svg',
  'test_timeout_svg',
  'test_reflection_svg'
];

ActorResultConst.NONE = 0;
ActorResultConst.NA = 1;
ActorResultConst.N_IMPL = 2;
ActorResultConst.SUCCESS = 3;
ActorResultConst.N_EXEC = 4;
ActorResultConst.INTERRUPT = 5;
ActorResultConst.FAILURE = 6;
ActorResultConst.ERROR = 7;
ActorResultConst.TIMEOUT = 8;
ActorResultConst.REFLECTION = 9;

const _ActorResultConst_tableNameResultIds = new Map();
const _ActorResultConst_tableNameClasses = new Map();
for(let i = 0; i < ActorResultConst.tableNameResults.length; ++i) {
  _ActorResultConst_tableNameResultIds.set(ActorResultConst.tableNameResults[i], i);
  _ActorResultConst_tableNameClasses.set(ActorResultConst.tableNameResults[i], ActorResultConst.classes[i]);
}

module.exports = ActorResultConst;
