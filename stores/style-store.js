
'use strict';

import { DataActionStyleGet } from '../actions/action-style/data-action-style';
import StoreBaseData from 'z-abs-corelayer-client/store/store-base-data';


class StyleStore extends StoreBaseData {
  constructor() {
    super({
      style: new Map([
        ['th.style_store_login_display_parents', {display:'none'}],
        ['td.style_store_login_display_parents', {display:'none'}],
        ['th.style_store_login_display_dependencies', {display:'none'}],
        ['td.style_store_login_display_dependencies', {display:'none'}]
      ]),
      stackStyles: new Map()
    });
    this.styleSheet = null;
    this.once('DataActionDialogFileVerifyOrCreate', () => {
      this.sendDataAction(new DataActionStyleGet());
    }, true);
    this.protocolStyleId = -1;
    this.styleInitFunction = null;
  }
  
  onActionStyleUpdate(action) {
    const cssRules = this.styleSheet.cssRules;
    for(let i = cssRules.length - 1; i > 0; --i) {
      if(action.selectorText === cssRules[i].selectorText) {
        action.onChange(cssRules[i].style);
        this.updateState({style: (style) => {
          style.set(action.selectorText, {display: cssRules[i].style.display});
        }});
        break;
      }
    }
  }
  
  onDataActionStyleGet(action) {
    const response = action.getResponse();
    if(response.isSuccess()) {
      const stackStyles = new Map(response.data);
      if(-1 !== this.protocolStyleId) {
        this.setStyleSheet(stackStyles, document.styleSheets[this.protocolStyleId]);
        this.updateState({stackStyles: {$set: stackStyles}});
      }
      else {
        this.styleInitFunction = () => {
          this.setStyleSheet(stackStyles, document.styleSheets[this.protocolStyleId]);
          this.updateContext(() => {
          this.updateState({stackStyles: {$set: stackStyles}});
          });
        }; 
      }
    }
  }
  
  setStyleSheet(stackStyles, styleSheet) {
    this.styleSheet = styleSheet;
    stackStyles.forEach((protocol, protocolKey) => {
      const classLine = `seq_dia_protocol_${protocolKey}`;
      styleSheet.insertRule(`line.${classLine} { stroke:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`text.${classLine} { fill:${protocol.textColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`text.${classLine}_protocol { fill:${protocol.textProtocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`rect.${classLine}_small { stroke:black;stroke-width:1;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`rect.${classLine}_big { stroke:black;stroke-width:2;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`circle.${classLine}_small { stroke:black;stroke-width:1;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`circle.${classLine}_small_fail { stroke:red;stroke-width:1;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`circle.${classLine}_big { stroke:black;stroke-width:2;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`div.${classLine}_inner { border:solid 2px ${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`div.${classLine}_header { background-color:${protocol.protocolBackgroundColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`rect.${classLine}_dimond { stroke:black;stroke-width:0.25;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`polygon.${classLine}_arrow { stroke:grey;stroke-width:1;fill:${protocol.protocolBackgroundColor}; }`, styleSheet.cssRules.length);
      styleSheet.insertRule(`polygon.${classLine}_small { stroke:black;stroke-width:1;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);     
      styleSheet.insertRule(`polygon.${classLine}_big { stroke:black;stroke-width:2;fill:${protocol.protocolColor}; }`, styleSheet.cssRules.length);
    });
    styleSheet.insertRule(`th.style_store_login_display_parents {display:none;}`, styleSheet.cssRules.length);
    styleSheet.insertRule(`td.style_store_login_display_parents {display:none;}`, styleSheet.cssRules.length);
    styleSheet.insertRule(`th.style_store_login_display_dependencies {display:none;}`, styleSheet.cssRules.length);
    styleSheet.insertRule(`td.style_store_login_display_dependencies {display:none;}`, styleSheet.cssRules.length);
  }
}

StyleStore.self = new StyleStore();

window.onload = () => {
  const css = document.createElement('style');
  css.type = 'text/css';
  document.getElementsByTagName("head")[0].appendChild(css);
  StyleStore.self.protocolStyleId = document.styleSheets.length - 1;
  if(StyleStore.self.styleInitFunction) {
    StyleStore.self.styleInitFunction();
    StyleStore.self.styleInitFunction = null;
  }
};


module.exports = StyleStore.self;
