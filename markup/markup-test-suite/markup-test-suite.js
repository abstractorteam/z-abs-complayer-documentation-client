
'use strict';

const DataTestSuite = require('../../data/data-test-suite/data-test-suite');
const DataTestSuiteAbstraction = require('../../data/data-test-suite/data-test-suite-abstraction');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupTestSuite {
  markupName() {
    return 'ts';
  }
  
  stringify(dataTestSuite, stringifyEmptyTables = true) {
    const dataTestSuiteTs = dataTestSuite.ts;
    const markup = MarkupTableKeyValue.stringify(new DataTestSuiteAbstraction(), dataTestSuiteTs.abstractions, stringifyEmptyTables);
    return markup;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    const abstractions = MarkupTableKeyValue.parse(DataTestSuiteAbstraction, objectTables.objects);
    const dataTestSuite = new DataTestSuite(abstractions);
    return {
      success: true,
      ts: dataTestSuite
    };
  }
}


module.exports = new MarkupTestSuite();
