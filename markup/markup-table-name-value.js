
'use strict';

const MarkupTablesParser = require('./markup-tables-parser');


class MarkupTableNameValue {
  stringify(tables) {
    let markup = '';
    tables.forEach((table, index) => {
      let sizes = this._calculateSizes(table.rows);
      markup += `${this._generateMarkup(table.name, table.rows, sizes)}${index === tables.length - 1 ? '' : '\n\n'}`;
    });
    return markup;
  }

  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(objectTables.success) {
      let tables = objectTables.objects.map((tableRows) => {
        let name = tableRows[0].columns[0]; 
        tableRows.shift();
        let rows = tableRows.map((tableRow) => {
          return {
            name: tableRow.columns[0],
            value: tableRow.columns[1]
          };
        });
        return {
          name: name,
          rows: rows
        };
      });
      return {
        success: true,
        tables: tables
      }; 
    }
    else {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
  }
  
  _calculateSizes(rows) {
    let sizes = [0, 0];  
    rows.forEach((row) => {
      this._calculateSize(sizes, row);
    });
    return sizes;
  }
  
  _calculateSize(sizes, row) {
    if(undefined !== row.name) {
      if(sizes[0] < row.name.length) {
        sizes[0] = row.name.length;
      }
    }
    if(undefined !== row.value) {
      if(sizes[1] < row.value.length) {
        sizes[1] = row.value.length;
      }
    }
  }
  
  _generateMarkup(tableName, tableRows, sizes) {
    let totalColumnSize = sizes.length - 1 + sizes.reduce((prev, curr) => prev + curr);
    let formattedtableRows = [];
    if(tableName.length <= totalColumnSize) {
      tableName += ' '.repeat(totalColumnSize - tableName.length);
      formattedtableRows = tableRows.map((row) => {
        return this._generateMarkupRow(row, sizes);
      });
    }
    let markup = `|${tableName}|`;
    formattedtableRows.forEach((row, index) => {
      markup += `\n|${row.name}|${row.value}|`;
    });
    return markup;
  }
  
  _generateMarkupRow(row, sizes) {
    return {
      name: this._generateMarkupColumn(row.name, 0, sizes),
      value: this._generateMarkupColumn(row.value, 1, sizes)
    };
  }
  
  _generateMarkupColumn(column, index, sizes) {
    if(undefined !== column) {
      return column +  ' '.repeat(sizes[index] - column.length);
    }
    else {
       return ' '.repeat(sizes[index]);
    }
  }
}

module.exports = new MarkupTableNameValue();
