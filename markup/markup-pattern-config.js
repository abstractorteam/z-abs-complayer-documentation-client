
'use strict';


class MarkupPatternConfig {
  stringify(config) {
    let stringified = '';
    const keys = Reflect.ownKeys(config);
    stringified += 'Config(';
    if(1 <= keys.length) {
      stringified += `${keys[0]}: ${Reflect.get(config, keys[0])}`;
      for(let i = 1; i < keys.length; ++i) {
        stringified += `, ${keys[i]}: ${Reflect.get(config, keys[i])}`;
      }
    }
    stringified += ')\n\n';  
    return stringified;
  }
  
  parse(markup, parent) {
    const start = markup.indexOf('(', 0);
    if(-1 === start) {
      return;
    }
    const stop = markup.indexOf(')', start + 1);
    const configParameters = markup.substring(start + 1, -1 !== stop ? stop : undefined).split(',');
    if(null !== configParameters) {
      const config = {};
      configParameters.forEach((configParameter) => {
        const parameters = configParameter.split(':');
        if(2 === parameters.length) {
          const parameterName = parameters[0].trim();
          const parameterValue = parameters[1].trim();
          const numberValue = Number.parseInt(parameterValue);
          if(!Number.isNaN(numberValue)) {
            parent.addConfig(parameterName, numberValue);
          }
          else if('true' === parameterValue) {
            parent.addConfig(parameterName, true);
          }
          else if('false' === parameterValue) {
            parent.addConfig(parameterName, false);
          }
          else {
            parent.addConfig(parameterName, parameterValue);
          }
        }
      });
    }
  }
}

module.exports = new MarkupPatternConfig();
