
'use strict';

const MarkupTablesParser = require('./markup-tables-parser');
const MarkupPatternConfig = require('./markup-pattern-config');
const DataTableConfig = require('../data/data-table/data-table-config');


class MarkupTable {
  markupName() {
    return 'table';
  }

  stringify(data) {
    let stringified = '';
    stringified += MarkupPatternConfig.stringify(data.config);
    data.objects.forEach((objects, index) => {
      const sizes = this._calculateSizes(objects);
      stringified += `${this._generateMarkup(objects, sizes)}`;
    });
    return stringified;
  }
  
  parse(markup) {
    const dataTableConfig = new DataTableConfig();
    const table = MarkupTablesParser.parse(markup, (rows) => {
      rows.forEach((row) => {
        const trimRow = row.trim();
        if(trimRow.startsWith('Config')) {
          MarkupPatternConfig.parse(trimRow, dataTableConfig);
        }
      });
      return rows;
    });
    table.config = dataTableConfig;
    return table;
  }
  
  _calculateSizes(objects) {
    const sizes = [];  
    for(let i = 1; i < objects.length; ++i) {
      const object = objects[i];
      const columns = object.columns.length;
      for(let j = sizes.length; j < columns; ++j) {
        sizes.push(0);
      }
      for(let j = 0; j < columns; ++j) {
        if(sizes[j] < object.columns[j].length) {
          sizes[j] = object.columns[j].length;
        }
      }
    }
    return sizes;
  }
  
  _generateMarkup(objects, sizes) {
    const totalColumnSize = sizes.length - 1 + sizes.reduce((prev, curr) => prev + curr);
    let markupTableName = 1 <= objects.length ? (1 <= objects[0].columns.length ? objects[0].columns[0] : '') : '';
    const markupTableHeadings = 2 <= objects.length ? objects[1].columns : [];
    if(markupTableName.length <= totalColumnSize) {
      markupTableName += ' '.repeat(totalColumnSize - markupTableName.length);
    }
    this._generateMarkupRow(markupTableHeadings, sizes);
    let markup = `|${markupTableName}|\n|${markupTableHeadings.join('|')}|`;
    for(let i = 2; i < objects.length; ++i) {
      this._generateMarkupRow(objects[i].columns, sizes);
      markup += `\n|${objects[i].columns.join('|')}|`;
    }
    return markup;
  }
  
  _generateMarkupRow(row, sizes) {
    row.forEach((value, index, array) => {
      if(undefined !== value) {
        array[index] = value +  ' '.repeat(sizes[index] - value.length);
      }
      else {
         array[index] = ' '.repeat(sizes[index]);
      }
    });
  }
}


module.exports = new MarkupTable();
