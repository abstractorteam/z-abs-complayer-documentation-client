
'use strict';

const MarkupTableKeyValueStringify = require('./markup-table-key-value-stringify');
const MarkupTableKeyValueParse = require('./markup-table-key-value-parse');


class MarkupTableKeyValue {
  stringify(dataObject, dataValues, stringifyEmptyTables) {
    return MarkupTableKeyValueStringify.stringify(dataObject, dataValues, stringifyEmptyTables);
  }

  parse(dataObjects, markup) {
    return MarkupTableKeyValueParse.parse(dataObjects, markup);
  }
}

module.exports = new MarkupTableKeyValue();
