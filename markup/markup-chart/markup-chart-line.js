
'use strict';

const DataChartAxis = require('../../data/data-chart/data-chart-axis');
const DataChartGrid = require('../../data/data-chart/data-chart-grid');
const DataChartLine = require('../../data/data-chart/data-chart-line/data-chart-line');
const DataChartLineConfig = require('../../data/data-chart/data-chart-line/data-chart-line-config');
const MarkupPatternData = require('../markup-pattern-data');
const MarkupPatternConfig = require('../markup-pattern-config');
const MarkupPatternFunction = require('../markup-pattern-function');


class MarkupChartLine {
  markupName() {
    return 'chart-line';
  }
  
  stringify(dataChartLine) {
    let stringified = '';
    if('' !== dataChartLine.title) {
      stringified += `${MarkupChartLine.TAG_TITLE} ${dataChartLine.title}\n`;
    }
    stringified += MarkupPatternConfig.stringify(dataChartLine.dataChartLineConfig);
    if(1 <= dataChartLine.axes.length) {
      stringified += MarkupPatternFunction.stringify('Axis', dataChartLine.axes);
    }
    if(null !== dataChartLine.grid) {
      stringified += MarkupPatternFunction.stringify('Grid', dataChartLine.grid);
    }
    if(1 <= dataChartLine.data.size) {
      stringified += MarkupPatternData.stringify(dataChartLine.data);
    }
    return stringified;
  }
  
  parse(markup) {
    const rows = this._getRows(markup);
    const dataChartLine = new DataChartLine();
    rows.forEach((row) => {
      this._parseRow(dataChartLine, row);
    });
    return dataChartLine;
  }
  
  _parseRow(dataChartLine, row) {
    const trimRow = row.trim();
    if(trimRow.startsWith(MarkupChartLine.TAG_TITLE)) {
      dataChartLine.addTitle(trimRow.substr(MarkupChartLine.TAG_TITLE_LENGTH).trim());
    }
    else if(trimRow.startsWith('Axis')) {
      MarkupPatternFunction.parse(trimRow, (...parameters) => {
        dataChartLine.addAxis(new DataChartAxis(...parameters));
      });
    }
    else if(trimRow.startsWith('Grid')) {
      MarkupPatternFunction.parse(trimRow, (...parameters) => {
        dataChartLine.setGrid(new DataChartGrid(...parameters));
      });
    }
    else if(trimRow.startsWith('Line')) {
      MarkupPatternData.parse(trimRow, dataChartLine);
    }
    else if(trimRow.startsWith('Config')) {
      MarkupPatternConfig.parse(trimRow, dataChartLine);
    }
  }
  
  _getRows(markup) {
    let index = 0;
    let nextIndex = 0;
    let rows = [];
    let length = markup.length;
    while(index < length) {
      nextIndex = markup.indexOf('\n', index);
      if(-1 !== nextIndex) {
        rows.push(markup.substring(index, nextIndex).trim());
        index = nextIndex + 1;
      }
      else {
        rows.push(markup.substring(index, length).trim());
        index = length;
      }
    }
    return rows;
  }
}

MarkupChartLine.TAG_TITLE = 'Title:';
MarkupChartLine.TAG_TITLE_LENGTH = MarkupChartLine.TAG_TITLE.length;


module.exports = new MarkupChartLine();
