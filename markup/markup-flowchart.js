
'use strict';

const DataFlowchart = require('../data/data-flowchart/data-flowchart');
const DataFlowchartBlock = require('../data/data-flowchart/data-flowchart-block');
const DataFlowchartConnection = require('../data/data-flowchart/data-flowchart-connection');


class MarkupFlowchart {
  markupName() {
    return 'flow';
  }
  
  stringify(flowchart) {
    let stringified = '';
    if('' !== flowchart.title) {
      stringified += `${MarkupFlowchart.TAG_TITLE} ${flowchart.title}\n`;
    }
    if(1 <= flowchart.blockRows.length) {
      for(let i = 0; i < flowchart.blockRows.length; ++i) {
        stringified += 'Blocks[';
        const firstBlock = flowchart.blockRows[i][0];
        stringified += firstBlock.name;
        if('' !== firstBlock.name && (undefined !== firstBlock.type || undefined !== firstBlock.data || undefined !== firstBlock.css || undefined !== firstBlock.link)) {
          stringified += `(${firstBlock.type ? firstBlock.type : ''}:${firstBlock.data ? firstBlock.data : ''}:${firstBlock.css ? firstBlock.css : ''}:${firstBlock.link ? firstBlock.link : ''})`;
        }
        for(let j = 1; j < flowchart.blockRows[i].length; ++j) {
          const block = flowchart.blockRows[i][j];
          stringified += `, ${block.name}`;
          if('' !== block.name && (undefined !== block.type || undefined !== block.data || undefined !== block.css || undefined !== block.link)) {
            stringified += `(${block.type ? block.type : ''}:${block.data ? block.data : ''}:${block.css ? block.css : ''}:${block.link ? block.link : ''})`;
          }
        }
        stringified += ']\n';
      }
    }
    else {
      stringified += 'Blocks[]';
    }
    for(let i = 0; i < flowchart.connections.length; ++i) {
      const connection = flowchart.connections[i];
      if(-1 !== connection.fromXIndex && -1 !== connection.fromYIndex && -1 !== connection.toXIndex && -1 !== connection.toYIndex) {
        const fromBlock = this._getBlockFromName(flowchart, connection.fromBlockName);
        const toBlock = this._getBlockFromName(flowchart, connection.toBlockName);
        if(null !== fromBlock && null !== toBlock) {
          stringified += `\n${fromBlock.name} ${this._stringifyConnectionType(connection)} ${toBlock.name}[${connection.type}]: ${connection.data}`;
          continue;
        }
      }
      stringified += `\n${connection.data}`
    }
    return stringified;
  }
  

  parse(markup) {
    const rows = this._getRows(markup);
    const dataFlowchart = new DataFlowchart();
    rows.forEach((row) => {
      this._parseRow(dataFlowchart, row);
    });
    return dataFlowchart;
  }
  
  _parseRow(dataFlowchart, row) {
    const trimRow = row.trim();
    if(trimRow.startsWith(MarkupFlowchart.TAG_TITLE)) {
      dataFlowchart.setTitle(trimRow.substr(MarkupFlowchart.TAG_TITLE_LENGTH).trim());
    }
    else if(trimRow.startsWith('Blocks')) {
      let index = 0;
      while(true) {
        const start = trimRow.indexOf('[', index);
        if(-1 === start) {
          break;
        }
        const stop = trimRow.indexOf(']', start + 1);
        const blockNames = trimRow.substring(start + 1, -1 !== stop ? stop : undefined).split(',');
        if(null !== blockNames) {
          dataFlowchart.addBlockRow();
          blockNames.forEach((blockName) => {
            let trimBlock = '';
            if(blockName.startsWith(',') && blockName.endsWith(',')) {
              trimBlock = blockName.substring(1, blockName.length - 1).trim();
            }
            else {
              trimBlock = blockName.trim();
            }
            const startType = trimBlock.indexOf('(');
            const stopType = trimBlock.indexOf(')', startType + 1);
            if(-1 !== startType && -1 !==  stopType) {
              const blockParameters = trimBlock.substring(startType + 1, -1 !== stopType ? stopType : undefined).split(':');
              dataFlowchart.addBlock(new DataFlowchartBlock(trimBlock.substring(0, startType), ...blockParameters));
            }
            else {
              dataFlowchart.addBlock(new DataFlowchartBlock(trimBlock));
            }
          });
        }
        if(-1 === stop) {
          break;
        }
        else {
          index = stop;
        }
      }
    }
    else {
      let type = 'unknown';
      let data = '';
      let typeStartIndex = trimRow.indexOf('[');
      let typeStopIndex = trimRow.indexOf(']');
      if(-1 !== typeStartIndex && -1 !== typeStopIndex) {
        const types = trimRow.slice(typeStartIndex + 1, typeStopIndex).match(/([^\s,]+)/g);
        if(types && 1 === types.length) {
          type = types[0];
        }
      }
      const dataIndex = trimRow.indexOf(':');
      if(-1 !== dataIndex) {
        data = trimRow.substring(dataIndex + 1, trimRow.length).trim();
      }
      let connectionTypeIndex = this._parseConnectionTypeIndex(trimRow);
      if(-1 !== connectionTypeIndex) {
        const connectionType = this._parseConnectionType(trimRow, connectionTypeIndex);
        const from = trimRow.substring(0, connectionTypeIndex).trim();
        const to = trimRow.substring(connectionTypeIndex + connectionType.size, typeStartIndex).trim();
        const fromBlock = this._getBlockFromName(dataFlowchart, from);
        const toBlock = this._getBlockFromName(dataFlowchart, to);
        if(null !== fromBlock && null !== toBlock) {
          dataFlowchart.addConnection(new DataFlowchartConnection(data, type, connectionType.type, fromBlock, toBlock));
        }
        else {
          dataFlowchart.addConnection(new DataFlowchartConnection(trimRow, type, connectionType.type, fromBlock, toBlock));
        }
      }
    }
  }
  
  _getRows(markup) {
    let index = 0;
    let nextIndex = 0;
    let rows = [];
    let length = markup.length;
    while(index < length) {
      nextIndex = markup.indexOf('\n', index);
      if(-1 !== nextIndex) {
        rows.push(markup.substring(index, nextIndex).trim());
        index = nextIndex + 1;
      }
      else {
        rows.push(markup.substring(index, length).trim());
        index = length;
      }
    }
    return rows;
  }
    
  _stringifyConnectionType(connection) {
    if('normal' === connection.connectionType) {
      return '=>';
    }
    else if('normal-bi-directional' === connection.connectionType) {
      return '<=>';
    }
    else if('part' === connection.connectionType) {
      return '->';
    }
    else if('part-bi-directional' === connection.connectionType) {
      return '<->';
    }
    else if('connect' === connection.connectionType) {
      return '-o';
    }
    else if('disconnect' === connection.connectionType) {
      return '-x';
    }
  }
  
  _parseConnectionTypeIndex(row) {
    let index = row.indexOf('<=>');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('=>');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('<->');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('->');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('-o');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('-x');
    if(-1 !== index) {
      return index;
    }
    return -1;
  }
  
  _parseConnectionType(row, index) {
    if('=>' === row.substr(index, 2)) {
      return {
        type: 'normal',
        size: 2
      };
    }
    else if('<=>' === row.substr(index, 3)) {
      return {
        type: 'normal-bi-directional',
        size: 3
      };
    }
    else if('->' === row.substr(index, 2)) {
      return {
        type: 'part',
        size: 2
      };
    }
    else if('<->' === row.substr(index, 3)) {
      return {
        type: 'part-bi-directional',
        size: 3
      };
    }
    else if('-o' === row.substr(index, 2)) {
      return {
        type: 'connect',
        size: 2
      };
    }
    else if('-x' === row.substr(index, 2)) {
      return {
        type: 'disconnect',
        size: 2
      };
    }
    return '';
  }
  
  _getBlockFromName(dataFlowchart, blockName) {
    for(let i = 0; i < dataFlowchart.blockRows.length; ++i) {
      for(let j = 0; j < dataFlowchart.blockRows[i].length; ++j) {
        if(dataFlowchart.blockRows[i][j].name === blockName) {
          return dataFlowchart.blockRows[i][j];
        }
      }
    }
    return null;
  }
}

MarkupFlowchart.TAG_TITLE = 'Title:';
MarkupFlowchart.TAG_TITLE_LENGTH = MarkupFlowchart.TAG_TITLE.length;


module.exports = new MarkupFlowchart();
