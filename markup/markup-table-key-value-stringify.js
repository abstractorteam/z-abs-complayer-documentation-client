
'use strict';


class MarkupTableKeyValueStringify {
  stringify(dataObject, dataValues, stringifyEmptyTables) {
    const markupTableName = dataObject.getName();
    const markupTableHeadings = dataObject.getHeadings();
    let markupTableRows = this._getMarkupTableRows(markupTableHeadings, dataValues);
    if(0 === markupTableRows.length && !stringifyEmptyTables) {
      return '';
    }
    const sizes = this._calculateSizes(markupTableName, markupTableHeadings, markupTableRows);
    return this._generateMarkup(markupTableName, markupTableHeadings, markupTableRows, sizes);
  }
  
  _getMarkupTableRows(markupTableHeadings, dataValues) {
    const markupTableRows = [];
    if(undefined !== dataValues) {
      dataValues.forEach((dataValue) => {
        let markupTableRow = [];
        markupTableRows.push(markupTableRow);
        markupTableHeadings.forEach((key) => {
          markupTableRow.push(Reflect.get(dataValue, key));
        });
      });
    }
    return markupTableRows;
  }
  
  _calculateSizes(markupTableName, markupTableHeadings, markupTableRows) {
    const sizes = Array(markupTableHeadings.length).fill(0);  
    this._calculateSize(sizes, markupTableHeadings);
    markupTableRows.forEach((row) => {
      this._calculateSize(sizes, row);
    });
    return sizes;
  }
  
  _calculateSize(sizes, array) {
    array.forEach((value, index) => {
      if(undefined !== value) {
        if(sizes[index] < value.length) {
          sizes[index] = value.length;
        }
      }
    });
  }
  
  _generateMarkup(markupTableName, markupTableHeadings, markupTableRows, sizes) {
    const totalColumnSize = sizes.length - 1 + sizes.reduce((prev, curr) => prev + curr);
    if(markupTableName.length <= totalColumnSize) {
      markupTableName += ' '.repeat(totalColumnSize - markupTableName.length);
      this._generateMarkupRow(markupTableHeadings, sizes);
      markupTableRows.forEach((row) => {
        this._generateMarkupRow(row, sizes);
      });
      markupTableRows.push(new Array(sizes.length));
      this._generateMarkupRow(markupTableRows[markupTableRows.length - 1], sizes);
    }
    let markup = `|${markupTableName}|\n|${markupTableHeadings.join('|')}|`;
    markupTableRows.forEach((row) => {
      markup += `\n|${row.join('|')}|`;
    });
    return markup;
  }
  
  _generateMarkupRow(row, sizes) {
    for(let index = 0; index < row.length; ++index) {
      if(undefined !== row[index]) {
        row[index] = row[index] +  ' '.repeat(sizes[index] - row[index].length);
      }
      else {
         row[index] = ' '.repeat(sizes[index]);
      }
    };
  }
}

module.exports = new MarkupTableKeyValueStringify();
