
'use strict';

const DataDependencies = require('../../data/data-dependencies/data-dependencies');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupDependencies {
  stringify(dataDependencies) {
    return `\n${MarkupTableKeyValue.stringify(new DataDependencies(), dataDependencies, true)}\n`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      dependencies: MarkupTableKeyValue.parse(DataDependencies, objectTables.objects)
    };
  }
}

module.exports = new MarkupDependencies();
