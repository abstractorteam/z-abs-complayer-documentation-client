
'use strict';

const DataSequenceDiagram = require('../data/data-sequence-diagram/data-sequence-diagram');
const DataSequenceDiagramNode = require('../data/data-sequence-diagram/data-sequence-diagram-node');
const DataSequenceDiagramMessage = require('../data/data-sequence-diagram/data-sequence-diagram-message');


class MarkupSequenceDiagram {
  markupName() {
    return 'seq';
  }
    
  stringify(sequenceDiagram) {
    let stringified = '';
    if('' !== sequenceDiagram.title) {
      stringified += `${MarkupSequenceDiagram.TAG_TITLE}${sequenceDiagram.title}\nNodes[`;
    }
    else {
      stringified += 'Nodes[';
    }
    if(1 <= sequenceDiagram.nodes.length) {
      stringified += sequenceDiagram.nodes[0].name;
      for(let i = 1; i < sequenceDiagram.nodes.length; ++i) {
        stringified += `, ${sequenceDiagram.nodes[i].name}`;
      }
    }
    stringified += ']';
    for(let i = 0; i < sequenceDiagram.messages.length; ++i) {
      let message = sequenceDiagram.messages[i];
      if(-1 !== message.fromIndex && -1 !== message.toIndex) {
        stringified += `\n${sequenceDiagram.nodes[message.fromIndex].name} ${this._stringifyMessageType(message)} ${sequenceDiagram.nodes[message.toIndex].name}[${message.type}]: ${message.data}`;
      }
      else {
        stringified += `\n${message.data}`
      }
    }
    return stringified;
  }
  
  parse(markup) {
    const rows = this._getRows(markup);
    const dataSequenceDiagram = new DataSequenceDiagram();
    rows.forEach((row) => {
      this._parseRow(dataSequenceDiagram, row);
    });
    return dataSequenceDiagram;
  }
  
  _parseRow(dataSequenceDiagram, row) {
    const trimRow = row.trim();
    if(trimRow.startsWith(MarkupSequenceDiagram.TAG_TITLE)) {
      dataSequenceDiagram.setTitle(trimRow.substr(MarkupSequenceDiagram.TAG_TITLE_LENGTH));
    }
    else if(trimRow.startsWith('Nodes')) {
      const nodeNames = trimRow.slice(trimRow.indexOf('[') + 1, trimRow.indexOf(']')).match(/([^\s,]+)/g);
      if(null !== nodeNames) {
        nodeNames.forEach((nodeName) => {
          dataSequenceDiagram.addNode(new DataSequenceDiagramNode(nodeName));
        });
      }
    }
    else {
      let type = 'unknown';
      let data = '';
      let typeStartIndex = trimRow.indexOf('[');
      let typeStopIndex = trimRow.indexOf(']');
      if(-1 !== typeStartIndex && -1 !== typeStopIndex) {
        let types = trimRow.slice(typeStartIndex + 1, typeStopIndex).match(/([^\s,]+)/g);
        if(types && 1 === types.length) {
          type = types[0];
        }
      }
      let dataIndex = trimRow.indexOf(':');
      if(-1 !== dataIndex) {
        data = trimRow.substring(dataIndex + 1, trimRow.length).trim();
      }
      let messageTypeIndex = this._parseMessageTypeIndex(trimRow);
      if(-1 !== messageTypeIndex) {
        let messageType = this._parseMessageType(trimRow, messageTypeIndex);
        let from = trimRow.substring(0, messageTypeIndex).trim();
        let to = trimRow.substring(messageTypeIndex + messageType.size, typeStartIndex).trim();
        let fromIndex = this._getNodeIndex(dataSequenceDiagram, from);
        let toIndex = this._getNodeIndex(dataSequenceDiagram, to);
        if(-1 !== fromIndex && -1 !== toIndex) {
          dataSequenceDiagram.addMessage(new DataSequenceDiagramMessage(data, type, messageType.type, fromIndex, toIndex));
        }
        /*else {
          dataSequenceDiagram.addMessage(new DataSequenceDiagramMessage(trimRow, type, messageType.type, fromIndex, toIndex));
        }*/
      }
    }
  }
  
  _getRows(markup) {
    let index = 0;
    let nextIndex = 0;
    const rows = [];
    const length = markup.length;
    while(index < length) {
      nextIndex = markup.indexOf('\n', index);
      if(-1 !== nextIndex) {
        rows.push(markup.substring(index, nextIndex).trim());
        index = nextIndex + 1;
      }
      else {
        rows.push(markup.substring(index, length).trim());
        index = length;
      }
    }
    return rows;
  }
    
  _stringifyMessageType(message) {
    if('normal' === message.messageType) {
      return '=>';
    }
    else if('normal-bi-directional' === message.messageType) {
      return '<=>';
    }
    else if('part' === message.messageType) {
      return '->';
    }
    else if('part-bi-directional' === message.messageType) {
      return '<->';
    }
    else if('connect' === message.messageType) {
      return '-o';
    }
    else if('disconnect' === message.messageType) {
      return '-x';
    }
  }
  
  _parseMessageTypeIndex(row) {
    let index = row.indexOf('<=>');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('=>');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('<->');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('->');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('-o');
    if(-1 !== index) {
      return index;
    }
    index = row.indexOf('-x');
    if(-1 !== index) {
      return index;
    }
    return -1;
  }
  
  _parseMessageType(row, index) {
    if('=>' === row.substr(index, 2)) {
      return {
        type: 'normal',
        size: 2
      };
    }
    else if('<=>' === row.substr(index, 3)) {
      return {
        type: 'normal-bi-directional',
        size: 3
      };
    }
    else if('->' === row.substr(index, 2)) {
      return {
        type: 'part',
        size: 2
      };
    }
    else if('<->' === row.substr(index, 3)) {
      return {
        type: 'part-bi-directional',
        size: 3
      };
    }
    else if('-o' === row.substr(index, 2)) {
      return {
        type: 'connect',
        size: 2
      };
    }
    else if('-x' === row.substr(index, 2)) {
      return {
        type: 'disconnect',
        size: 2
      };
    }
    return '';
  }
  
  _getNodeIndex(dataSequenceDiagram, nodeName) {
    return dataSequenceDiagram.nodes.findIndex((node) => {
      return node.name === nodeName;
    });
  }
}

MarkupSequenceDiagram.TAG_TITLE = 'Title: ';
MarkupSequenceDiagram.TAG_TITLE_LENGTH = MarkupSequenceDiagram.TAG_TITLE.length;


module.exports = new MarkupSequenceDiagram();
