
'use strict';

const DataTestDataLogGlobal = require('../../data/data-test-data/data-test-data-log-global');
const DataTestDataLogLocal = require('../../data/data-test-data/data-test-data-log-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupTestDataLog {
  stringify(dataTestDataGlobals, dataTestDataLocals) {
    return `\n${MarkupTableKeyValue.stringify(new DataTestDataLogLocal(), dataTestDataLocals, true)}\n\n${MarkupTableKeyValue.stringify(new DataTestDataLogGlobal(), dataTestDataGlobals, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataTestDataLogGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataTestDataLogLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupTestDataLog();
