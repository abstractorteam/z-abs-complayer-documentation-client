
'use strict';

const DataTestDataGeneralGlobal = require('../../data/data-test-data/data-test-data-general-global');
const DataTestDataGeneralLocal = require('../../data/data-test-data/data-test-data-general-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupTestDataGeneral {
  stringify(dataTestDataGlobals, dataTestDataLocals) {
    return `\n${MarkupTableKeyValue.stringify(new DataTestDataGeneralLocal(), dataTestDataLocals, true)}\n\n${MarkupTableKeyValue.stringify(new DataTestDataGeneralGlobal(), dataTestDataGlobals, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataTestDataGeneralGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataTestDataGeneralLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupTestDataGeneral();
