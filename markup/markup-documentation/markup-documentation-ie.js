
'use strict';


class MarkupDocumentationIe {
  constructor() {
    this.key = 'IE';
    this.type = 'markup_ie';
  }
  
  stringify(dataIe) {
    return `${this.key}=${dataIe}`;
  }
  
  parse(markup) {
    return markup;
  }
}


module.exports = new MarkupDocumentationIe() ;
