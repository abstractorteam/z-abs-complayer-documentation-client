
'use strict';


class MarkupDocumentationRef {
  constructor() {
    this.key = 'REF';
    this.type = 'markup_ref';
  }
  
  stringify(dataRef) {
    return `${this.key}=${dataRef.text}, ${dataRef.refName}`;
  }
  
  parse(markup) {
    const parameters = markup.split(',');
    parameters.forEach((parameter, index) => {
      parameters[index] = parameter.trim();
    });
    return {
      text: 1 <= parameters.length ? parameters[0] : 'Unknown',
      refName: 2 <= parameters.length ? parameters[1] : 'Unknown'
    };
  }
}


module.exports = new MarkupDocumentationRef() ;
