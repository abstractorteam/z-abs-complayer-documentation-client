
'use strict';

const MarkupParseHelper = require('../markup-parse-helper');
const DataHtmlAnchor = require('../../data/data-html/data-html-anchor');
const DataHtmlButton = require('../../data/data-html/data-html-button');
const DataHtmlBr = require('../../data/data-html/data-html-br');
const DataHtmlLog = require('../../data/data-html/data-html-log');
const DataHtmlLogStart = require('../../data/data-html/data-html-log-start');
const DataHtmlLogStop = require('../../data/data-html/data-html-log-stop');


class MarkupDocumentationHtml {
  markupName() {
    return 'object';
  }
  
  stringify(dataHtmls) {
    let stringified = '';
    dataHtmls.forEach((dataHtml) => {
      if('anchor' ===  dataHtml.type) {
        stringified += `\n[anchor](${dataHtml.link})`;
      }
      else if('button' === dataHtml.type) {
        if(undefined !== dataHtml.text) {
          stringified += `\n[button](${dataHtml.glyphicon}, ${dataHtml.text})`; 
        }
        else {
          stringified += `\n[button](${dataHtml.glyphicon})`; 
        }
      }
      else if('br' ===  dataHtml.type) {
        stringified += `\n[br]`;
      }
      else if('log' ===  dataHtml.type) {
        stringified += `\n[log](${dataHtml.logType}, ${dataHtml.date}, ${dataHtml.actor}, ${dataHtml.log}, ${dataHtml.fileName})`;
      }
      else if('log_start' ===  dataHtml.type) {
        stringified += `\n[log_start](${dataHtml.date})`;
      }
      else if('log_end' ===  dataHtml.type) {
        stringified += `\n[log_end](${dataHtml.result}, ${dataHtml.duration})`;
      }
    });
    return stringified;
  }
  
  parse(markup) {
    let objects = [];
    let rows = MarkupParseHelper.getRows(markup);
    rows.forEach((row) => {
      if(row.startsWith('[anchor]')) {
        this._parseAnchor(objects, row.trim());
      } 
      else if(row.startsWith('[button]')) {
        this._parseButton(objects, row.trim());
      }
      else if(row.startsWith('[br]')) {
        objects.push(new DataHtmlBr());
      }
      else if(row.startsWith('[log]')) {
        this._parseLog(objects, row.trim());
      }
      else if(row.startsWith('[log_start]')) {
        this._parseLogStart(objects, row.trim());
      }
      else if(row.startsWith('[log_end]')) {
        this._parseLogStop(objects, row.trim());
      }
    });
    return objects;
  }
  
  _parseAnchor(objects, row) {
    let parameterNames = row.slice(row.indexOf('(') + 1, row.lastIndexOf(')')).split(',');
    if(undefined !== parameterNames) {
      objects.push(new DataHtmlAnchor(...parameterNames));
    }
  }
  
  _parseButton(objects, row) {
    let parameterNames = row.slice(row.indexOf('(') + 1, row.lastIndexOf(')')).split(',');
    if(2 <= parameterNames.length) {
      parameterNames[1] = parameterNames[1].trim();
    }
    if(undefined !== parameterNames) {
      objects.push(new DataHtmlButton(...parameterNames));
    }
  }
  
  _parseLog(objects, row) {
    let parameterNames = row.slice(row.indexOf('(') + 1, row.lastIndexOf(')')).split(',');
    parameterNames.forEach((parameterName, index) => {
      parameterNames[index] = parameterName.trim();
    });
    if(undefined !== parameterNames) {
      objects.push(new DataHtmlLog(...parameterNames));
    }
  }

  _parseLogStart(objects, row) {
    let parameterNames = row.slice(row.indexOf('(') + 1, row.lastIndexOf(')')).split(',');
    parameterNames.forEach((parameterName, index) => {
      parameterNames[index] = parameterName.trim();
    });
    if(undefined !== parameterNames) {
      objects.push(new DataHtmlLogStart(...parameterNames));
    }
  }

  _parseLogStop(objects, row) {
    let parameterNames = row.slice(row.indexOf('(') + 1, row.lastIndexOf(')')).split(',');
    parameterNames.forEach((parameterName, index) => {
      parameterNames[index] = parameterName.trim();
    });
    if(undefined !== parameterNames) {
      objects.push(new DataHtmlLogStop(...parameterNames));
    }
  }
}


module.exports = new MarkupDocumentationHtml();
