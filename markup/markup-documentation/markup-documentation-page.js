
'use strict';

const MarkupChartLine = require('../markup-chart/markup-chart-line');
const MarkupDocumentationAnchor = require('./markup-documentation-anchor');
const MarkupDocumentationCode = require('./markup-documentation-code');
const MarkupSequenceDiagram = require('../markup-sequence-diagram');
const MarkupLab = require('../markup-lab');
const MarkupFlowchart = require('../markup-flowchart');
const MarkupNodeDiagram = require('../markup-node-diagram');
const MarkupDocumentationDiv = require('./markup-documentation-div');
const MarkupDocumentationEmbed = require('./markup-documentation-embed');
const MarkupDocumentationIe = require('./markup-documentation-ie');
const MarkupDocumentationRef = require('./markup-documentation-ref');
const MarkupDocumentationResult = require('./markup-documentation-result');
const MarkupDocumentationHtml = require('./markup-documentation-html');
const MarkupDocumentationImage = require('./markup-documentation-image');
const MarkupDocumentationLab = require('./markup-documentation-lab');
const MarkupDocumentationLocalNote  = require('./markup-documentation-local-note');
const MarkupDocumentationStateMachine = require('./markup-documentation-state-machine');
const MarkupTable = require('../markup-table');
const MarkupTestCase = require('../markup-test-case/markup-test-case');
const MarkupTestSuite = require('../markup-test-suite/markup-test-suite');


class MarkupDocumentationPage {
  constructor() {
    this.markups = new Map();
    this.markupKeySimples = new Map();
    this.markupTypeSimples = new Map();
    this._addSimple(MarkupDocumentationLocalNote);
    this._addSimple(MarkupDocumentationAnchor);
    this._addSimple(MarkupDocumentationEmbed);
    this._addSimple(MarkupDocumentationIe);
    this._addSimple(MarkupDocumentationRef);
    this._addSimple(MarkupDocumentationResult);
    this._addSimple(MarkupDocumentationDiv);
    this._addSimple(MarkupDocumentationImage);
    this.markupTypeComplex = [];
    this._addComplex(MarkupChartLine);
    this._addComplex(MarkupDocumentationCode);
    this._addComplex(MarkupFlowchart);
    this._addComplex(MarkupDocumentationHtml);
    this._addComplex(MarkupLab);
    this._addComplex(MarkupNodeDiagram);
    this._addComplex(MarkupSequenceDiagram);
    this._addComplex(MarkupDocumentationStateMachine);
    this._addComplex(MarkupTable);
    this._addComplex(MarkupTestCase);
    this._addComplex(MarkupTestSuite);
  }
  
  stringify(page) {
    if(undefined !== page) {
      const stringified = page.map((value) => {
        if('markup' === value.type) {
          return value.value;
        }
        else if(MarkupDocumentationPage.RAW_NAME === value.type) {
          return `${MarkupDocumentationPage.RAW_START}${value.value.replace(new RegExp('```', 'g'), '\\`\\`\\`')}${MarkupDocumentationPage.PART}`
        }
        else if(MarkupDocumentationPage.ESCAPE_NAME === value.type) {
          return `${MarkupDocumentationPage.ESCAPE_START}\n${this._removeLastLineBreak(this._escapeStringify(value.value))}\n${MarkupDocumentationPage.PART}`
        }
        else {
          for(let i = 0; i < this.markupTypeComplex.length; ++i) {
            const markupObject = this.markupTypeComplex[i];
            const markupName = markupObject.markupName();
            if(markupName === value.type) {
              return `${MarkupDocumentationPage.PART}${markupName}\n${markupObject.stringify(value.value, false)}\n${MarkupDocumentationPage.PART}`
            }
          }
          const markupObject = this.markupTypeSimples.get(value.type);
          if(markupObject) {
            return `${MarkupDocumentationPage.SIMPLE_START}${markupObject.stringify(value.value)}${MarkupDocumentationPage.SIMPLE_STOP}`;
          }
        }
      });
      return `${stringified.join('')}`;
    }
    else {
      return '';
    }
  }
  
  parse(markup) {
    if(undefined !== markup) {
      return this._getParse(markup);
    }
    else {
      return [];
    }
  }
  
  _getParse(markup, fromIndex = 0, searchIndex = 0, foundParts = []) {
    const indexStart = markup.indexOf(MarkupDocumentationPage.PART, searchIndex);
    const indexSimpleStart = markup.indexOf(MarkupDocumentationPage.SIMPLE_START, searchIndex);
    if ((indexStart < indexSimpleStart && -1 !== indexStart) || (-1 !== indexStart && -1 === indexSimpleStart)) {
      let indexStop = markup.indexOf(MarkupDocumentationPage.PART, indexStart + MarkupDocumentationPage.PART_LENGTH);
      if(-1 === indexStop) {
        indexStop = indexStart + MarkupDocumentationPage.PART_LENGTH;
      }
      if(markup.substr(indexStart, MarkupDocumentationPage.RAW_LENGTH, indexStop).startsWith(MarkupDocumentationPage.RAW_START)) {
        if(fromIndex !== indexStart) {
          this._setParseValue(foundParts, markup, fromIndex, indexStart);
        }
        const value = markup.substring(indexStart + MarkupDocumentationPage.RAW_START_LENGTH, indexStop);
        foundParts.push({
          type: MarkupDocumentationPage.RAW_NAME,
          value: value,
          lines: value.split('\n').length
        });
        fromIndex = indexStop + MarkupDocumentationPage.PART_LENGTH;
        return this._getParse(markup, fromIndex, indexStop + MarkupDocumentationPage.PART_LENGTH, foundParts);
      }
      else if(markup.substr(indexStart, MarkupDocumentationPage.ESCAPE_START_LENGTH, indexStop).startsWith(MarkupDocumentationPage.ESCAPE_START)) {
        if(fromIndex !== indexStart) {
          this._setParseValue(foundParts, markup, fromIndex, indexStart);
        }
        const stringValue = markup.substring(indexStart + MarkupDocumentationPage.ESCAPE_START_LENGTH, indexStop);
        const value = this._removeFirstLineBreak(this._regExpFixReplace(stringValue));
        foundParts.push({
          type: MarkupDocumentationPage.ESCAPE_NAME,
          value: value,
          lines: stringValue.split('\n').length
        });
        fromIndex = indexStop + MarkupDocumentationPage.PART_LENGTH;
        return this._getParse(markup, fromIndex, indexStop + MarkupDocumentationPage.PART_LENGTH, foundParts);
      }
      else {
        for(let i = 0; i < this.markupTypeComplex.length; ++i) {
          const markupObject = this.markupTypeComplex[i];
          const markupName = markupObject.markupName();
          const startLength = MarkupDocumentationPage.PART_LENGTH + markupName.length;
          if(markup.substr(indexStart, startLength).startsWith(MarkupDocumentationPage.PART + markupName)) {
            if(fromIndex !== indexStart) {
              this._setParseValue(foundParts, markup, fromIndex, indexStart);
            }
            const linesValue = markup.substring(indexStart, indexStop + MarkupDocumentationPage.PART_LENGTH);
            const stringValue = markup.substring(indexStart + startLength + 1, indexStop - 1);
            const value = markupObject.parse(stringValue);
            foundParts.push({
              type: markupName,
              value: value,
              lines: linesValue.split('\n').length
            });
            fromIndex = indexStop + MarkupDocumentationPage.PART_LENGTH;
            return this._getParse(markup, fromIndex, indexStop + MarkupDocumentationPage.PART_LENGTH, foundParts);
          }
        }
      }
      return this._getParse(markup, fromIndex, indexStop + MarkupDocumentationPage.PART_LENGTH, foundParts);
    }
    else if ((indexSimpleStart < indexStart && -1 !== indexSimpleStart) || (-1 !== indexSimpleStart && -1 === indexStart)) {
      let indexSimpleStop = markup.indexOf(MarkupDocumentationPage.SIMPLE_STOP, indexSimpleStart + MarkupDocumentationPage.SIMPLE_START_LENGTH);
      let simple = markup.substring(indexSimpleStart + MarkupDocumentationPage.SIMPLE_START_LENGTH, indexSimpleStop);
      let simpleKeyStopIndex = simple.indexOf('=');
      let key = simple;
      let json = '';
      if(-1 !== simpleKeyStopIndex) {
        key = simple.substring(0, simpleKeyStopIndex);
        json = simple.substring(simpleKeyStopIndex + 1);
      }
      let markupObject = this.markupKeySimples.get(key);
      if(markupObject) {
        if(fromIndex !== indexSimpleStart) {
          this._setParseValue(foundParts, markup, fromIndex, indexSimpleStart);
        }
        foundParts.push({
          type: markupObject.type,
          value: markupObject.parse(json, indexSimpleStop),
          lines: 1
        });
        fromIndex = indexSimpleStop + MarkupDocumentationPage.SIMPLE_STOP_LENGTH;
        return this._getParse(markup, fromIndex, fromIndex, foundParts);
      }
      return this._getParse(markup, fromIndex, indexSimpleStart + MarkupDocumentationPage.SIMPLE_START_LENGTH, foundParts);
    }
    else {
      this._setParseValue(foundParts, markup, fromIndex);
      return foundParts;
    }
  }
  
  _setParseValue(foundParts, markup, start, stop) {
    const value = markup.substring(start, stop);
    if(0 !== value.length) {
      foundParts.push({
        type: 'markup',
        value: value,
        lines: value.split('\n').length
      });
    }
  }
  
  _regExpFixReplace(s, start = 0) {
    let index = s.indexOf('\\`\\`\\`');
    if(-1 == index) {
      return s;
    }
    return this._regExpFixReplace(`${s.substr(0, index)}\`\`\`${s.substring(index + 6)}`, index + 6 + 1);
  }
  
  _escapeStringify(s, start=0) {
    let index = s.indexOf('```');
    if(-1 == index) {
      return s;
    }
    return this._escapeStringify(`${s.substr(0, index)}\\\`\\\`\\\`${s.substring(index + 3)}`, index + 3 + 1);
  }
  
  _removeFirstLineBreak(s) {
    if(s.startsWith('\n')) {
      return s.substring(1);
    }
    else {
      return s;
    }
  }

  _removeLastLineBreak(s) {
    if(s.endsWith('\n')) {
      return s.substring(0, s.length - 1);
    }
    else {
      return s;
    }
  }
  
  _addSimple(markupObject) {
    this.markupKeySimples.set(markupObject.key, markupObject);
    this.markupTypeSimples.set(markupObject.type, markupObject);
  }
  
  _addComplex(markupObject) {
    this.markupTypeComplex.push(markupObject);
  }
}

MarkupDocumentationPage.PART = '```';
MarkupDocumentationPage.PART_LENGTH = MarkupDocumentationPage.PART.length;

MarkupDocumentationPage.SIMPLE_START = '[[';
MarkupDocumentationPage.SIMPLE_START_LENGTH = MarkupDocumentationPage.SIMPLE_START.length;

MarkupDocumentationPage.SIMPLE_STOP = ']]';
MarkupDocumentationPage.SIMPLE_STOP_LENGTH = MarkupDocumentationPage.SIMPLE_STOP.length;

MarkupDocumentationPage.RAW_NAME = 'raw';
MarkupDocumentationPage.RAW_START = MarkupDocumentationPage.PART + MarkupDocumentationPage.RAW_NAME;
MarkupDocumentationPage.RAW_START_LENGTH = MarkupDocumentationPage.RAW_START.length;

MarkupDocumentationPage.ESCAPE_NAME = 'escape';
MarkupDocumentationPage.ESCAPE_START = MarkupDocumentationPage.PART + MarkupDocumentationPage.ESCAPE_NAME;
MarkupDocumentationPage.ESCAPE_START_LENGTH = MarkupDocumentationPage.ESCAPE_START.length;


module.exports = new MarkupDocumentationPage();
