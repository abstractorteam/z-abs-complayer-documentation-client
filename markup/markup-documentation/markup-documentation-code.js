
'use strict';


class MarkupDocumentationCode {
  markupName() {
    return 'javascript';
  }
  
  stringify(code) {
    return code;
  }
  
  parse(markup) {
    return markup;
  }
}


module.exports = new MarkupDocumentationCode();
