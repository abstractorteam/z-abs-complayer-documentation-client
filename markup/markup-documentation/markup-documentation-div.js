
'use strict';

import GuidGenerator from 'z-abs-corelayer-cs/guid-generator';


class MarkupDocumentationDiv {
  constructor() {
    this.key = 'DIV';
    this.type = 'markup_div';
  }
  
  stringify(dataAnchor) {
    return `${this.key}={"id":"${dataAnchor.id}","style":${JSON.stringify(dataAnchor.style)}}`;
  }
  
  parse(markup) {
    let dataAnchor = null;
    try {
      dataAnchor = JSON.parse(markup);
    } 
    catch(err) {}
    if(dataAnchor && dataAnchor.id) {
      return dataAnchor;
    }
    return {
      id: GuidGenerator.create(),
      style: ''
    };
  }
}


module.exports = new MarkupDocumentationDiv() ;
