
'use strict';


class MarkupDocumentationEmbed {
  constructor() {
    this.key = 'EMBED';
    this.type = 'markup_embed';
  }
  
  stringify(dataEmbed) {
    return `${this.key}=${dataEmbed.path}`;
  }
  
  parse(markup) {
    return {
      path: markup
    };
  }
}


module.exports = new MarkupDocumentationEmbed() ;
