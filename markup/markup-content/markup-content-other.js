
'use strict';

const DataContentOtherGlobal = require('../../data/data-content/data-content-other-global');
const DataContentOtherLocal = require('../../data/data-content/data-content-other-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupContentOther {
  stringify(dataContentGlobals, dataContentLocals) {
    return `\n${MarkupTableKeyValue.stringify(new DataContentOtherLocal(), dataContentLocals, true)}\n\n${MarkupTableKeyValue.stringify(new DataContentOtherGlobal(), dataContentGlobals, true)}\n`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataContentOtherGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataContentOtherLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupContentOther();
