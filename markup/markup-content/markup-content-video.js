
'use strict';

const DataContentVideoGlobal = require('../../data/data-content/data-content-video-global');
const DataContentVideoLocal = require('../../data/data-content/data-content-video-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupContentVideo {
  stringify(dataContentGlobals, dataContentLocals) {
    return `\n${MarkupTableKeyValue.stringify(new DataContentVideoLocal(), dataContentLocals, true)}\n\n${MarkupTableKeyValue.stringify(new DataContentVideoGlobal(), dataContentGlobals, true)}\n`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataContentVideoGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataContentVideoLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupContentVideo();
