
'use strict';

const DataContentTextGlobal = require('../../data/data-content/data-content-text-global');
const DataContentTextLocal = require('../../data/data-content/data-content-text-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupContentText {
  stringify(dataContentGlobals, dataContentLocals) {
    return `\n${MarkupTableKeyValue.stringify(new DataContentTextLocal(), dataContentLocals, true)}\n\n${MarkupTableKeyValue.stringify(new DataContentTextGlobal(), dataContentGlobals, true)}\n`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataContentTextGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataContentTextLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupContentText();
