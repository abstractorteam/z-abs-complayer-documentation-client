
'use strict';

const DataContentImageGlobal = require('../../data/data-content/data-content-image-global');
const DataContentImageLocal = require('../../data/data-content/data-content-image-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupContentImage {
  stringify(dataContentGlobals, dataContentLocals) {
    return `\n${MarkupTableKeyValue.stringify(new DataContentImageLocal(), dataContentLocals, true)}\n\n${MarkupTableKeyValue.stringify(new DataContentImageGlobal(), dataContentGlobals, true)}\n`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataContentImageGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataContentImageLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupContentImage();
