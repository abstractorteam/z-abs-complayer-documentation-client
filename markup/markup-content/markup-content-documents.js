
'use strict';

const DataContentDocumentsGlobal = require('../../data/data-content/data-content-documents-global');
const DataContentDocumentsLocal = require('../../data/data-content/data-content-documents-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupContentDocuments {
  stringify(dataContentGlobals, dataContentLocals) {
    return `\n${MarkupTableKeyValue.stringify(new DataContentDocumentsLocal(), dataContentLocals, true)}\n\n${MarkupTableKeyValue.stringify(new DataContentDocumentsGlobal(), dataContentGlobals, true)}\n`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataContentDocumentsGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataContentDocumentsLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupContentDocuments();
