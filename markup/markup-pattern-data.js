
'use strict';

const DataData = require('../data/data-data');


class MarkupPatternData {
  stringify(datas) {
    let stringified = '';
    datas.forEach((data, key) => {
      stringified += `${key}`;
      data.datas.forEach((d) => {
        stringified += `[${d}]`;
      });
      stringified += '\n';
    });
    return stringified;
  }
  
  parse(markup, dataParent, numberType = 'int') {
    let index = 0;
    const startData = markup.indexOf('[');
    if(-1 === startData) {
      return;
    }
    const name = markup.substring(0, startData);
    let data = dataParent.getData(name);
    if(undefined === data) {
      data = new DataData(name, numberType);
      dataParent.addData(name, data);
    }
    while(true) {
      const startData = markup.indexOf('[', index);
      if(-1 === startData) {
        break;
      }
      const stopData = markup.indexOf(']', startData + 1);
      const parameters = markup.substring(startData + 1, -1 !== stopData ? stopData : undefined).split(',');
      data.addData(...parameters);
      if(-1 === stopData) {
        break;
      }
      else {
        index = stopData;
      }
    }
  }
}

module.exports = new MarkupPatternData();
