
'use strict';

const DataAddressesSutInterfaceGlobal = require('../../data/addresses/data-addresses-sut-interface-global');
const DataAddressesSutInterfaceLocal = require('../../data/addresses/data-addresses-sut-interface-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesSutInterface {
  stringify(dataAddressesSutInterfaceGlobal, dataAddressesSutInterfaceLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesSutInterfaceLocal(), dataAddressesSutInterfaceLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesSutInterfaceGlobal(), dataAddressesSutInterfaceGlobal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesSutInterfaceGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesSutInterfaceLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupAddressesSutInterface();
