
'use strict';

const DataAddressesClientInterfaceGlobal = require('../../data/addresses/data-addresses-client-interface-global');
const DataAddressesClientInterfaceLocal = require('../../data/addresses/data-addresses-client-interface-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesClientInterface {
  stringify(dataAddressesClientInterfaceGlobal, dataAddressesClientInterfaceLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesClientInterfaceLocal(), dataAddressesClientInterfaceLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesClientInterfaceGlobal(), dataAddressesClientInterfaceGlobal, true)}\n`;
  }
  
  parse(markup) {
    const objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesClientInterfaceGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesClientInterfaceLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupAddressesClientInterface();
