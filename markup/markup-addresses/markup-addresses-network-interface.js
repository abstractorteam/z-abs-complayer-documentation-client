
'use strict';

const DataAddressesNetworkInterfaceGlobal = require('../../data/addresses/data-addresses-network-interface-global');
const DataAddressesNetworkInterfaceLocal = require('../../data/addresses/data-addresses-network-interface-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesNetworkInterface {
  stringify(dataAddressesNetworkInterfaceGlobal, dataAddressesNetworkInterfaceLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesNetworkInterfaceLocal(), dataAddressesNetworkInterfaceLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesNetworkInterfaceGlobal(), dataAddressesNetworkInterfaceGlobal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesNetworkInterfaceGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesNetworkInterfaceLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupAddressesNetworkInterface();
