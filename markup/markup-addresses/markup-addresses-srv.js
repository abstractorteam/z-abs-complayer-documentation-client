
'use strict';

const DataAddressesSrvGlobal = require('../../data/addresses/data-addresses-srv-global');
const DataAddressesSrvLocal = require('../../data/addresses/data-addresses-srv-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesSrv {
  stringify(dataAddressesSrvGlobal, dataAddressesSrvLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesSrvLocal(), dataAddressesSrvLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesSrvGlobal(), dataAddressesSrvGlobal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesSrvGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesSrvLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupAddressesSrv();
