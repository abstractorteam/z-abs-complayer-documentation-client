
'use strict';

const DataAddressesServerInterfaceGlobal = require('../../data/addresses/data-addresses-server-interface-global');
const DataAddressesServerInterfaceLocal = require('../../data/addresses/data-addresses-server-interface-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesServerInterface {
  stringify(dataAddressesServerInterfaceGlobal, dataAddressesServerInterfaceLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesServerInterfaceLocal(), dataAddressesServerInterfaceLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesServerInterfaceGlobal(), dataAddressesServerInterfaceGlobal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesServerInterfaceGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesServerInterfaceLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupAddressesServerInterface();
