
'use strict';

const DataAddressesPortsGlobal = require('../../data/addresses/data-addresses-ports-global');
const DataAddressesPortsLocal = require('../../data/addresses/data-addresses-ports-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesPorts {
  stringify(dataAddressesPortsGlobal, dataAddressesPortsLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesPortsLocal(), dataAddressesPortsLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesPortsGlobal(), dataAddressesPortsGlobal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesPortsGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesPortsLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupAddressesPorts();
