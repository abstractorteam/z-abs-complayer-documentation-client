
'use strict';

const DataAddressesDstGlobal = require('../../data/addresses/data-addresses-dst-global');
const DataAddressesDstLocal = require('../../data/addresses/data-addresses-dst-local');
const MarkupTableKeyValue = require('../markup-table-key-value');
const MarkupTablesParser = require('../markup-tables-parser');


class MarkupAddressesDst {
  stringify(dataAddressesDstGlobal, dataAddressesDstLocal) {
    return `\n${MarkupTableKeyValue.stringify(new DataAddressesDstLocal(), dataAddressesDstLocal, true)}\n\n${MarkupTableKeyValue.stringify(new DataAddressesDstGlobal(), dataAddressesDstGlobal, true)}\n`;
  }
  
  parse(markup) {
    let objectTables = MarkupTablesParser.parse(markup);
    if(!objectTables.success) {
      return {
        success: false,
        rows: objectTables.rows
      };
    }
    return {
      success: true,
      globals: MarkupTableKeyValue.parse(DataAddressesDstGlobal, objectTables.objects),
      locals: MarkupTableKeyValue.parse(DataAddressesDstLocal, objectTables.objects)
    };
  }
}

module.exports = new MarkupAddressesDst();
